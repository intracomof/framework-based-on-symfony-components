<?php
use Application\Console;
date_default_timezone_set('UTC');
require(__DIR__ . '/vendor/autoload.php');

$application = new Console();
$application->run($_SERVER['argv']);


