<?php
date_default_timezone_set('UTC');
header('Content-Type: text/html; charset=utf-8');

use LogosV8\Container;
use Application\Application;

require_once __DIR__.'/../vendor/autoload.php';

$app = new Application(Container::getConfig('routing'));

$app->run();