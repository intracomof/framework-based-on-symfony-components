/**
 * Moving filter to container
 * @param filterSelector
 * @param targetSelector
 * @param filterContainerSelector
 */
function moveGridFilter(filterSelector, targetSelector, filterContainerSelector) {
    filterSelector.appendTo(targetSelector);
    filterSelector.css({'float': 'right', 'max-width': '250px'});
    filterSelector.addClass('form-control');
    filterSelector.attr("placeholder", "Search");
    filterContainerSelector.remove();
}