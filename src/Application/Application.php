<?php

namespace Application;

use LogosV8\Container;
use Symfony\Component\Routing;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use LogosV8\Plugins\ACL\Auth;

/**
 * Class Application
 * @package Application
 */
class Application implements IApp
{
    protected $matcher;
    protected $controllerResolver;
    protected $argumentResolver;

    /**
     * Application constructor.
     * @param Routing\RouteCollection $routes
     */
    public function __construct(Routing\RouteCollection $routes)
    {
        Container::$session = new Session();
        Container::$session->start();

        $context = new Routing\RequestContext();
        $this->matcher = new Routing\Matcher\UrlMatcher($routes, $context);

        $this->controllerResolver = new ControllerResolver();
        $this->argumentResolver = new ArgumentResolver();
    }

    /**
     * @param Request $request
     * @return mixed|Response
     */
    public function handle($request)
    {
        $this->matcher->getContext()->fromRequest($request);

        try {

            Auth::authBySession($request);

            $request->attributes->add($this->matcher->match($request->getPathInfo()));
            $controller = $this->controllerResolver->getController($request);
            $arguments = $this->argumentResolver->getArguments($request, $controller);

            // check user's permissions
            /** @var \LogosV8\Controllers\AbstractController */
            $controller[0]->checkPermission($controller[1]);
            $controller[0]->setRequest($request);

            return call_user_func_array($controller, $arguments);

        } catch (ResourceNotFoundException $e) {
            return new Response('Not Found', 404);
        }
    }

    /**
     * @param Request|null $request
     */
    public function run($request = null)
    {
        if (null === $request) {
            $request = Request::createFromGlobals();
        }

        $response = $this->handle($request);
        $response->send();
    }
}