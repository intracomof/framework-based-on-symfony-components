<?php

namespace Application;

interface IApp
{
    public function run($request);

    public function handle($request);
}