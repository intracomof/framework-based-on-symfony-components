<?php

namespace Application;

use LogosV8\Container;

/**
 * Class Application
 * @package Application
 */
class Console implements IApp
{
    const COMMAND_NAMESPACE = 'LogosV8\\Command\\';
    const PREFIX_ACTION = 'action';

    /**
     * @param $request
     * @return bool|mixed
     */
    public function handle($request)
    {
        if (!empty($request[1])) {

            $route = $request[1];

            unset($request[0]);
            unset($request[1]);

            $controller = $this->createController($route);

            return call_user_func_array($controller, $request);
        }

        return false;
    }

    /**
     * @param $route
     * @return array
     */
    protected function createController($route)
    {
        if (false === strpos($route, '/')) {
            throw new \InvalidArgumentException(sprintf('Unable to find CLI command "%s".', $route));
        }

        list($class, $method) = explode('/', $route, 2);

        $class = Container::getConfig('console')['command_namespace'] . $class;
        $method = Container::getConfig('console')['prefix_action'] . $method;

        if (!class_exists($class)) {
            throw new \InvalidArgumentException(sprintf('Class "%s" does not exist.', $class));
        }

        return [$this->instantiateController($class), $method];
    }

    /**
     * Returns an instantiated controller.
     *
     * @param string $class A class name
     *
     * @return object
     */
    protected function instantiateController($class)
    {
        return new $class();
    }

    /**
     * @param $request
     * @return bool|mixed
     */
    public function run($request)
    {
        $result = $this->handle($request);

        return $result;
    }
}