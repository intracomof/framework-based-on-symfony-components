<?php

namespace LogosV8\Storage\Elastic;


use Elasticsearch\ClientBuilder;
use LogosV8\Container;
use LogosV8\Storage\AbstractStorage;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class Storage
 * @package LogosV8\Model\Elastic
 */
class ElasticStorage extends AbstractStorage
{
    /** Logger type name for elastic search */
    const TYPE_NAME = 'logger';

    static $index;

    static $type;

    public $client;

    public $params = [];

    /**
     * @var \Elasticsearch\Client $connection
     */
    protected  static $connection;

    /**
     * Storage constructor.
     */
    public function __construct()
    {
        self::$connection = ClientBuilder::create()
            ->setHosts(Container::getConfig('elasticsearch'))
            ->build();
        $this->client = self::$connection;
        $this->initParams();

    }

    /**
     * Инициализация индекса и типа
     */
    public function initParams()
    {
        $this->params = [
            'index' => static::$index,
            'type' => static::$type,
        ];
    }

    /**
     * Get Elastic php client
     *
     * @return \Elasticsearch\Client
     */
    public static function getClient()
    {
        return self::$connection;
    }

    /**
     * Upsert document
     * @param string $label
     * @param mixed $value
     * @param string $index
     * @param string $type
     * @param string $id
     */
    public function upsert($label, $value, $index, $type, $id = null)
    {
        $id = empty($id) ? uniqid('idx_') : $id;

        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id,
            'body' => [
                'script' => 'ctx._source.counter += count',
                'params' => [
                    'count' => 1
                ],
                'upsert' => [
                    'counter' => 1,
                    $label => $value
                ]
            ]
        ];

        $this->client->update($params);
    }

    /**
     * Вставка новой записи
     * @param array $data
     * @return
     */
    public function insert(array $data)
    {
        if(!empty($data['id'])){
            $this->params['id'] = $data['id'];
        }
        $this->params['body'] = $data;
        $this->client->index($this->params);
    }

    /**
     * Обновление данных
     * @param array $data
     * @param $id
     */
    public function update(array $data, $id)
    {
        $this->initParams();
        $this->params['id'] = $id;
        $this->params['body']['doc'] = $data;
        $this->client->update($this->params);

    }

    /**
     * Установка сортировки
     * @param $sort
     */
    public function setSort($sort)
    {
        if (!empty($sort) && is_array($sort)) {
            foreach ($sort as $key => $value) {
                $this->params['body']['sort'][$key] = $value;
            }

        }
    }

    public function setQuery($query)
    {
        $this->params['body']['query'] = $query;
    }

    /**
     * Поиск всех данных
     * @return array
     */
    public function findAll()
    {
        //$this->initParams();
        return $this->client->search($this->params);
    }

    /**
     * Поис по ИД
     * @param $id
     * @return array
     */
    public function findById($id)
    {
        $this->initParams();
        $this->params['id'] = $id;
        return $this->client->get($this->params);
    }

    /**
     * Установка лимита
     * @param $limit
     * @return $this
     */
    public function limit($limit)
    {
        $limit = intval($limit);
        if ($limit) {
            $this->params['size'] = $limit;
        }
        return $this;
    }

    /**
     * Установка агрегаций
     * @param $aggs
     * @return $this
     */
    public function setAggs($aggs)
    {
        if ($aggs) {
            $this->params['size'] = 0;
            //$this->params['type'] ='';
            $this->params['body']['aggs'] = $aggs;
        }
        return $this;
    }

    /**
     * Установка офсета
     * @param $offset
     * @return $this
     */
    public function offset($offset)
    {
        $offset = intval($offset);
        if ($offset) {
            $this->params['from'] = $offset;
        }
        return $this;
    }

    /**
     * Поиск по условию, после этого метода можно вызвать лимит или офсет а потом нужно вызвать all
     *  example find($params)->limit(2)->all
     * @param $params
     * @return $this
     */
    public function find($params)
    {
        $this->initParams();
        $this->params['body']['query']=$params;
        return $this;
    }

    /**
     * Возвращает все данные
     * @return array
     */
    public function all()
    {
        return $this->client->search($this->params);
    }

    /**
     * Удаляет данные по ИД
     * @param array $query
     * @param array $options
     * @return array
     */
    public function delete(array $query, array $options)
    {
        $this->initParams();
        $this->params['id'] = $query['_id'];
        return $this->client->delete($this->params);
    }

}