<?php

namespace LogosV8\Storage\Mongo;

use LogosV8\Storage\AbstractStorage;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class AbstractDbTable
 * @package Logos\Model\DbTable
 */
class MongoStorage extends AbstractStorage
{
    /** @var string default DB name */
    protected $db = 'logos';

    /** @var  string $_collection Name of collection */
    protected $collectionName;

    protected $collectionInstance;
    public $result;

    /**
     * @return \Doctrine\MongoDB\Collection
     */
    public function getCollection()
    {
        if(!$this->collectionInstance) {
            $this->collectionInstance = MongoConnection::getConnection()->selectCollection($this->db, $this->collectionName);
        }

        return $this->collectionInstance;
    }

    /**
     * @param array $params
     * @return \Doctrine\MongoDB\Cursor
     */
    public function find(array $params)
    {
        $result = $this->getCollection()->find($params);
        return $result;
    }

    public function all()
    {
        return iterator_to_array($this->result);
    }

    public function one()
    {
        $this->result->limit(1);
        return iterator_to_array($this->result);
    }

    public function count()
    {
        return $this->result->count();
    }

    public function limit()
    {

    }

    /**
     * @param array $params
     * @return ArrayCollection
     */
    public function findBy(array $params)
    {
        $result = $this->getCollection()->find($params);
        return iterator_to_array($result);
    }

    /**
     * @param array $params
     * @return ArrayCollection
     */
    public function findOneBy(array $params)
    {
        $result = $this->getCollection()->find($params);
        return iterator_to_array($result->limit(1));
    }

    /**
     * @param $id
     * @return \Doctrine\MongoDB\Cursor
     */
    public function findById($id)
    {
        $result = $this->getCollection()->find(['id' => (int)$id]);

        return iterator_to_array($result);
    }

    /**
     * @return \Doctrine\MongoDB\Cursor
     */
    public function findAll($limit = 0, $offset = 0, $sort = [])
    {
        $result = $this->getCollection()->find();

        return $result;
    }

    /**
     * @param array $data
     * @param array $options
     * @return array|bool
     */
    public function insert(array $data, array $options = [])
    {
        return $this->getCollection()->insert($data, $options);
    }

    /**
     * @param array $data
     * @param $query
     * @return array|bool
     */
    public function update(array $data, $query)
    {
        return $this->getCollection()->update($query, $data);
    }

    /**
     * @param array $query
     * @param array $options
     * @return array|bool
     */
    public function delete(array $query, array $options = [])
    {
        return $this->getCollection()->remove($query, $options);
    }

    /**
     * @param array $query
     * @param array $options
     * @return array|bool
     */
    public function upsert(array $query, array $options = [])
    {
        return $this->getCollection()->upsert($query, $options);
    }

    /**
     * @param array $data
     * @param $query
     * @param array $options
     * @return array|bool
     */
    public function updateMulti(array $data, $query, array $options = [])
    {
        return $this->getCollection()->update($query, $data, $options);
    }

    /**
     * @param int $start
     * @param int $length
     * @param array $query
     * @param array $orders
     * @return \Doctrine\MongoDB\Cursor
     */
    public function findOffset($start = 0, $length = 25, array $query = [], array $orders = [])
    {
        $sortings = [];
        $resultQuery = [];

        if (count($orders)) {
            foreach ($orders as $field => $type) {
                $sortings[$field] = ($type == 'asc')?1:-1;
            }
        }

        if (isset($query['or']) && count($query['or']))
            $resultQuery = array_merge($resultQuery, $this->orWhereFilter($query['or']));

        if (isset($query['and']) && count($query['and']))
            $resultQuery = array_merge($resultQuery, $this->andWhereFilter($query['and']));

        return $this->getCollection()->find($resultQuery)->skip($start)->limit($length)->sort($sortings);
    }

    /**
     * @param array $query
     * @param array $options
     * @return int
     */
    public function recordsTotal($query, array $options = [])
    {
        $resultQuery = [];
        if (isset($query['or']) && count($query['or']))
            $resultQuery = array_merge($resultQuery, $this->orWhereFilter($query['or']));

        if (isset($query['and']) && count($query['and']))
            $resultQuery = array_merge($resultQuery, $this->andWhereFilter($query['and']));

        return $this->getCollection()->count($resultQuery, $options);
    }

    /**
     * @param array $data
     * @return array
     */
    public function andWhereFilter(array $data)
    {
        $result = [];
        if (count($data)) {
            $generateAnd = [];
            foreach ($data as $field => $value) {
                $generateAnd[][$field] = $value;
            }

            $result['$and'] = $generateAnd;
        }

        return $result;
    }

    /**
     * @param array $data
     * @return array
     */
    public function orWhereFilter(array $data)
    {
        $result = [];
        if (count($data)) {
            $generateOr = [];
            foreach ($data as $field => $value) {
                $generateOr[][$field] = ['$regex' => $value];
            }

            $result['$or'] = $generateOr;
        }

        return $result;
    }
}
