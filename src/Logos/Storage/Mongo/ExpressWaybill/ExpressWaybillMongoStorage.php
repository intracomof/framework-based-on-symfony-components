<?php

namespace LogosV8\Storage\Mongo\ExpressWaybill;

use LogosV8\Storage\Mongo\MongoStorage;

/**
 * Class ExpressWaybillDbTable
 * @package Logos\Model\DbTable
 */
class ExpressWaybillMongoStorage extends MongoStorage
{
    protected $collectionName = 'ExpressWaybill';
}
