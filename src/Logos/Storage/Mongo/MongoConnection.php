<?php

namespace LogosV8\Storage\Mongo;

use LogosV8\Container;
use \Doctrine\MongoDB\Connection;

/**
 * Class AbstractDbTable
 * @package Logos\Model\DbTable
 */
class MongoConnection
{
    /** @var \Doctrine\MongoDB\Connection $_connection  */
    static $connection;

    /**
     * @return \Doctrine\MongoDB\Connection
     */
    public static function getConnection()
    {
        if(!self::$connection) {
            $connectionParams = new \MongoClient(Container::getConfig('mongo')['server']);
            self::$connection = new Connection($connectionParams);
            self::$connection->connect();
        }

        return self::$connection;
    }

}
