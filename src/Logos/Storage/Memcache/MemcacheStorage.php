<?php

namespace LogosV8\Storage\Memcache;

/**
 * Class MemcacheLogos
 * @package Logos\Library
 */
class MemcacheStorage
{
    const SAVING_FLAG_NAME = 'isSavedWithMapperCaching';
    const SAVING_DATA_NAME = 'data';

    /**
     * @var \Memcached|null
     */
    protected $_memcache;

    protected $_lifetime = 86400;

    /**
     * Sets the memcache instance to use.
     *
     * @param \Memcached $memcache
     *
     * @return void
     */
    public function setMemcache(\Memcached $memcache)
    {
        $this->_memcache = $memcache;
    }

    /**
     * Gets the memcache instance used by the cache.
     *
     * @return \Memcached
     */
    public function getMemcache()
    {
        return $this->_memcache;
    }

    /**
     * @param $id
     * @param int $value
     * @param int $ttl
     * @return int
     */
    public function increment($id, $value = 0, $ttl = 86400)
    {
        $result = 0;

        $value = (int)$value;
        if($this->getMemcache()){
            $result = $this->getMemcache()->increment($id, $value);

            if (false === $result) {
                $this->getMemcache()->add($id, $value, false, $ttl);
                $result = $value;
            }

        }

        return $result;
    }

    /**
     * @param $lifetime
     * @return $this
     */
    public function setLifetime($lifetime)
    {
        $this->_lifetime = (int)$lifetime;
        return $this;
    }

    /**
     * @return int
     */
    public function getLifetime()
    {
        return $this->_lifetime;
    }

    /**
     * @param $key
     * @return bool|mixed
     */
    public function loadKey($key)
    {
        $info = $this->getMemcache()->get($key);
        // If cache exists return it
        if (is_array($info) && isset($info[self::SAVING_FLAG_NAME]) && $info[self::SAVING_FLAG_NAME]) {
            return $info;
        }

        return false;
    }

    /**
     * @param $keys
     * @return array
     */
    public function loadMulti($keys)
    {
        return $this->getMemcache()->getMulti($keys);
    }

    /**
     * @param $key
     * @param $data
     * @param bool $leftTime
     * @return bool
     */
    public function saveKey($key, $data, $leftTime = false)
    {
        $info = [
            self::SAVING_FLAG_NAME => true,
            self::SAVING_DATA_NAME => $data
        ];

        if($leftTime === false){
            $leftTime = 600;//10 min
        }

        $this->getMemcache()->set($key, $info, false, $leftTime);

        return true;
    }

    /**
     * @param $key
     */
    public function removeKey($key)
    {
        $this->getMemcache()->delete($key);
    }
}