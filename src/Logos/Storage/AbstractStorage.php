<?php

namespace LogosV8\Storage;

/**
 * Interface IStorage
 * @package LogosV8\Storage
 */
abstract class AbstractStorage
{
    public function __construct(){}


    /**
     * @param $id
     * @return mixed
     */
    public function findById($id){}

    /**
     * @return array|bool
     */
    public function findAll(){}

    /**
     * @param array $data
     * @return bool
     */
    public function insert(array $data){}

    /**
     * @param array $data
     * @param $query
     * @return mixed
     */

    public function update(array $data, $query){}


    /**
     * @param array $query
     * @param array $options
     * @return mixed
     */
    public function delete(array $query, array $options){}

}