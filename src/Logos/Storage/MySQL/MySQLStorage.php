<?php

namespace LogosV8\Storage\MySQL;

use LogosV8\Container;
use \Doctrine\DBAL\Configuration;
use \Doctrine\DBAL\DriverManager;
use \Doctrine\DBAL\Query\QueryBuilder;
use LogosV8\Storage\AbstractStorage;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class AbstractDbTable
 * @package Logos\Model\DbTable
 */
class MySQLStorage extends AbstractStorage
{

    /** @var \Doctrine\DBAL\Connection $connection  */
    protected $connection = null;

    protected $dbName = null;

    /**
     * @param bool $new
     * @return \Doctrine\DBAL\Connection
     */
    public function getConnection($new = false)
    {
        if($this->connection == null || $new) {
            $connectionParams = Container::getConfig('mysql');
            $config = new Configuration();
            $this->connection = DriverManager::getConnection($connectionParams, $config);
        }

        return $this->connection;
    }


    /**
     * @param QueryBuilder $queryBuilder
     * @param $fields
     * @return QueryBuilder
     */
    public function fillQueryBuilderForUpdate(QueryBuilder $queryBuilder, $fields)
    {
        foreach($fields as $column => $value) {
            $queryBuilder->set($column, ':' . $column);
            $queryBuilder->setParameter(':' . $column, $value);
        }

        return $queryBuilder;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param $fields
     * @return QueryBuilder
     */
    public function fillQueryBuilderForInsert(QueryBuilder $queryBuilder, $fields)
    {
        $i = 0;
        foreach($fields as $column => $value) {
            $queryBuilder->setValue($column, '?');
            $queryBuilder->setParameter($i++, $value);
        }

        return $queryBuilder;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param $fields
     * @return QueryBuilder
     */
    public function fillAndWhere(QueryBuilder $queryBuilder, $fields)
    {
        if(count($fields) > 0) {
            foreach($fields as $column => $value) {
                $queryBuilder->where($column . '= :' . $column);
                $queryBuilder->setParameter(':' . $column, $value);
            }
        }

        return $queryBuilder;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        $queryBuilder = $this->getConnection()->createQueryBuilder();
        $result = $queryBuilder
            ->select("*")
            ->from($this->dbName)
            ->where("id = :id")
            ->setParameter(':id', $id)
            ->execute()
            ->fetch();

        return $result;
    }

    /**
     * @return mixed
     */
    public function findAll()
    {
        $queryBuilder = $this->getConnection()->createQueryBuilder();
        $result = $queryBuilder
            ->select("*")
            ->from($this->dbName)
            ->execute()
            ->fetchAll();

        return $result;
    }

    /**
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function find(array $params)
    {
        if (!count($params))
            throw new \Exception('Cont find params in MySql query.');

        $where = "true";
        foreach ($params as $field => $item) {
            $where .= " AND ".$field." ".(($item['eq'])?$item['eq']:'=')." '".$item['value']."'";
        }
        $queryBuilder = static::getConnection()->createQueryBuilder();
        $result = $queryBuilder
            ->select("*")
            ->from(static::$dbName)
            ->where($where)
            ->execute()
            ->fetch();

        return $result;
    }

    /**
     * @param array $data
     * @param $query
     * @return bool
     */
    public function update(array $data, $query)
    {
        if(count($data) > 0) {

            $queryBuilder = $this->getConnection()->createQueryBuilder();

            $queryBuilder->update($this->dbName);

            $queryBuilder = $this->fillQueryBuilderForUpdate($queryBuilder, $data);

            static::fillAndWhere($queryBuilder, $query);

            $queryBuilder->execute();
        }

        return true;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function insert(array $data)
    {
        if(count($data) > 0) {

            $queryBuilder = $this->getConnection()->createQueryBuilder();

            $queryBuilder->insert($this->dbName);

            $queryBuilder = $this->fillQueryBuilderForInsert($queryBuilder, $data);

            $queryBuilder->execute();
        }

        return true;
    }

    /**
     * @param array $query
     * @param array $options
     * @return bool
     */
    public function delete(array $query, array $options = [])
    {
        $queryBuilder = $this->getConnection()->createQueryBuilder();

        $queryBuilder->delete($this->dbName);

        $queryBuilder
            ->where('id = :id')
            ->setParameter(':id', $query['id'])
            ->execute();

        return true;
    }
}