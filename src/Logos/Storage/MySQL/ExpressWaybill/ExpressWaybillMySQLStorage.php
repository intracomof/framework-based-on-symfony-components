<?php

namespace LogosV8\Storage\MySQL\ExpressWaybill;

use LogosV8\Storage\MySQL\MySQLStorage;

/**
 * Class ExpressWaybillDbTable
 * @package Logos\Model\DbTable
 */
class ExpressWaybillMySQLStorage extends MySQLStorage
{
    protected $dbName = 'express_waybill';

    /**
     * @param $ewNum
     * @return \Doctrine\DBAL\Driver\Statement|int
     */
    public function fetchExpressWayBillByEwNum($ewNum)
    {
        return self::getConnection()->createQueryBuilder()
            ->select("*")
            ->from($this->dbName)
            ->where("ew_num = :ew_num")
            ->setParameter(':ew_num', $ewNum)
            ->execute()
            ->fetch();
    }

}