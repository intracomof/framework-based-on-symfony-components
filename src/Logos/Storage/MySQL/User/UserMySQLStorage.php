<?php

namespace LogosV8\Storage\MySQL\User;

use LogosV8\Storage\MySQL\MySQLStorage;

/**
 * Class ExpressWaybillDbTable
 * @package Logos\Model\DbTable
 */
class UserMySQLStorage extends MySQLStorage
{
    protected $dbName = 'users';

    /**
     * @param $login
     * @return mixed
     */
    public function fetchUserByLoginName($login)
    {
        return self::getConnection()->createQueryBuilder()
            ->select("*")
            ->from($this->dbName)
            ->where("login_name = :login_name")
            ->setParameter(':login_name', $login)
            ->execute()
            ->fetch();
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function findById($userId)
    {
        return self::getConnection()->createQueryBuilder()
            ->select("*")
            ->from($this->dbName)
            ->where("user_id = :user_id")
            ->setParameter(':user_id', $userId)
            ->execute()
            ->fetch();
    }

}