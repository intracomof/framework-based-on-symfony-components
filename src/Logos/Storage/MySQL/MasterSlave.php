<?php

namespace LogosV8\Storage\MySQL;

use Doctrine\DBAL\Connections\MasterSlaveConnection;

/**
 * Class MasterSlave
 * @package LogosV8\Storage\MySQL
 */
class MasterSlave extends MasterSlaveConnection
{
    protected $_badSlaves = [];

    /** @var \Memcached $_badServersStorage */
    protected $_badServersStorage = null;

    /**
     * @return \Memcached
     */
    protected function getBadServersStorage()
    {
        if($this->_badServersStorage == null) {

            $connectionParams = $this->getParams()['memcache_bad_servers_list'];
            $server = $connectionParams['server'];
            $port = $connectionParams['port'];

            $memcache = new \Memcached();
            $memcache->addServer($server, $port);

            $this->_badServersStorage = $memcache;
        }

        return $this->_badServersStorage;
    }

    /**
     * Connects to a specific connection.
     *
     * @param string $connectionName
     *
     * @return \Doctrine\DBAL\Driver
     */
    protected function connectTo($connectionName)
    {
        $params = $this->getParams();
        $badSlaves = $this->fetchBadSlaves($params['slaves']);

        /**
         * Если слейв лежит, то убираем его из списка для подключения
         */
        foreach($badSlaves as $host => $server) {
            unset($params['slaves'][$host]);
            unset($params['weight_paramss'][$host]);
        }

        /**
         *  Если живые слейвы закончились, то слейвом становится мастер
         */
        if(count($params['slaves']) == 0) {
            array_push($params['slaves'], $params['master']);
        }

        $driverOptions = isset($params['driverOptions']) ? $params['driverOptions'] : array();
        $connectionParams = $this->chooseConnectionConfiguration($connectionName, $params);
        $user = isset($connectionParams['user']) ? $connectionParams['user'] : null;
        $password = isset($connectionParams['password']) ? $connectionParams['password'] : null;

        try {
            $connect = $this->_driver->connect($connectionParams, $user, $password, $driverOptions);
        } catch (\Exception $exception) {
            $this->addBadSlave($connectionParams['host'], $params);
            $connect = $this->connectTo($connectionName);
        }

        return $connect;
    }

    /**
     * @param string $connectionName
     * @param array  $params
     *
     * @return mixed
     */
    protected function chooseConnectionConfiguration($connectionName, $params)
    {
        if ($connectionName === 'master') {
            return $params['master'];
        }

        $rand = rand($params['min_weight'], $params['max_weight']);

        $serverParams = [];

        foreach($params['weight_params'] as $serverKey => $weight) {
            if($rand >= $weight[0] && $rand < $weight[1]) {
                if($serverKey == 'master') {
                    $serverParams = $params['master'];
                } else {
                    $serverParams = (isset($params['slaves'][$serverKey]))?$params['slaves'][$serverKey]:false;
                }
                break;
            }
        }

        if(empty($serverParams)) {
            $serverParams = $params['master'];
        }

        return $serverParams;
    }

    /**
     * @param $slaves
     * @return array
     */
    protected function fetchBadSlaves($slaves)
    {
        $badSlaves = $this->getBadServersStorage()->getMulti(array_keys($slaves));
        $this->_badSlaves = array_merge($this->_badSlaves, $badSlaves);

        return $this->_badSlaves;
    }

    /**
     * @param $host
     * @param $params
     */
    protected function addBadSlave($host, $params)
    {
        $this->_badSlaves[$host] = $host;
        $this->getBadServersStorage()->set($host, $host, $params['memcache_bad_servers_list']['ttl']);
    }
}