<?php

namespace LogosV8\Plugins\ACL;

class Roles
{
    const GUEST = 1;
    const USER = 2;
    const ADMIN = 3;
    const ROOT = 4;
    const API = 5;

    static $roles = [
        self::GUEST,
        self::USER,
        self::ADMIN,
        self::ROOT,
        self::API
    ];
}