<?php

namespace LogosV8\Plugins\ACL;

use LogosV8\Container;
use LogosV8\Models\User\UserModel;
use Symfony\Component\HttpFoundation\Request;

class Auth
{
    /**
     * Авторизация пользователя по сессии
     *
     * @param Request $request
     */
    public static function authBySession(Request $request)
    {
        $tokens = Container::getConfig('web')['api']['tokens'];
        $headerIterator = $request->headers->getIterator();

        $apiMethod = ($headerIterator->offsetExists('apimethod'))?$headerIterator->offsetGet('apimethod')[0]:false;
        $apiToken = ($headerIterator->offsetExists('apitoken'))?$headerIterator->offsetGet('apitoken')[0]:false;

        $user = new UserModel();
        $user->setRole(Roles::GUEST);

        Container::$user = $user;

        if($userId = Container::$session->get('uid')) {
            $user = Container::get('user_mysql_mapper')->findById($userId);
            $user->setRole(Roles::USER);
            if($user) {
                Container::$user = $user;
            }
        }
        else if ($apiMethod && $apiToken
            && in_array($apiMethod, array_keys($tokens)) && $apiToken == $tokens[$apiMethod]) {
            $user->setRole(Roles::API);
            Container::$user = $user;
        }

        if($user->getRole() == Roles::GUEST && $request->getRequestUri() != '/login') {
            header("Location: /login");
        }
    }
}