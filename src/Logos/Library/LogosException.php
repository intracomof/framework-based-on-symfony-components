<?php

namespace LogosV8\Library;
use \Exception as Exception;

/**
 * Class LogosException
 * @package Logos\Library
 */
class LogosException extends Exception
{
    const TYPE_LOG = 'log';
    const TYPE_EXCEPTION = 'exception';

    const THROW_NONE                        = 101;
    const THROW_MAPPER_ERROR                = 102;
    const THROW_REQUIREMENT_FIELDS_ERROR    = 103;
    const THROW_CONFIG_NOT_FOUND            = 104;
    const THROW_PERMISSION_DENIED           = 105;
    const THROW_BAD_PASSWORD                = 106;
    const THROW_BAD_HASH                    = 107;

    protected static $errors = [
        self::THROW_NONE                            => ['error'=>'[LogosException]: Not given exception id. ', 'code' => 1001],
        self::THROW_MAPPER_ERROR                    => ['error'=>'[LogosException][MapperError]: ', 'code' => 1002],
        self::THROW_REQUIREMENT_FIELDS_ERROR        => ['error'=>'[LogosException][RequirementFieldsError]: Requirement fields for inserting data: ', 'code' => 1003],
        self::THROW_CONFIG_NOT_FOUND                => ['error'=>'[LogosException][ConfigError]: Config not found. Path to config: ', 'code' => 1004],
        self::THROW_PERMISSION_DENIED               => ['error'=>'[LogosException][PermissionError]: Permission denied', 'code' => 1005],
        self::THROW_BAD_PASSWORD                    => ['error'=>'[LogosException][AuthError]: Password must be a string and cannot be empty', 'code' => 1006],
        self::THROW_BAD_HASH                        => ['error'=>'[LogosException][AuthError]: Hash is invalid.', 'code' => 1006],
    ];

    /**
     * @param $type
     * @return mixed
     */
    public static function getErrorCode($type)
    {
        return self::$errors[$type]['code'];
    }

    /**
     * LogosException constructor.
     * @param int $avalue
     * @param string $type
     * @param string $message
     * @throws Exception
     */
    function __construct($avalue = self::THROW_NONE, $type = self::TYPE_EXCEPTION, $message = '')
    {
        if(!isset(self::$errors[$avalue])) {
            throw new Exception(self::$errors[self::THROW_NONE]['error'], self::$errors[self::THROW_NONE]['code']);
        } else {
            if($type == self::TYPE_LOG) {
                error_log(self::$errors[$avalue]['error'] . $message);
            } else {
                throw new Exception(self::$errors[$avalue]['error'] . $message, self::$errors[$avalue]['code']);
            }
        }
    }

}