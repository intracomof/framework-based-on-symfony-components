<?php

namespace LogosV8\Library\Twig;

use LogosV8\Models\Manifest\Manifest;
use Symfony\Component\HttpFoundation\Request;
use LogosV8\Services\Traits\ValidateTrait;
use LogosV8\Library\Messages\Messages;

/**
 * Class TwigFunctions
 * @package LogosV8\Library\Twig
 */
class TwigFunctions {

    const WIDGET_DIR = 'widgets';

    private $twigService;
    private $messages;


    /**
     * TwigFunctions constructor.
     * @param $twig
     */
    public function __construct($twig)
    {
        $this->twigService = $twig;
        $this->messages = new Messages();
    }

    /**
     * @return mixed
     */
    public function run()
    {
        $this->twigService->addFunction(new \Twig_SimpleFunction('queueStatus', [$this, 'queueStatus']));
        $this->twigService->addFunction(new \Twig_SimpleFunction('varDump', [$this, 'varDump']));
        $this->twigService->addFunction(new \Twig_SimpleFunction('varPrint', [$this, 'varPrint']));
        $this->twigService->addFunction(new \Twig_SimpleFunction('isAssoc', [$this, 'isAssoc']));
        $this->twigService->addFunction(new \Twig_SimpleFunction('isArray', [$this, 'isArray']));
        $this->twigService->addFunction(new \Twig_SimpleFunction('isString', [$this, 'isString']));
        $this->twigService->addFunction(new \Twig_SimpleFunction('isArrayMulti', [$this, 'isArrayMulti']));
        $this->twigService->addFunction(new \Twig_SimpleFunction('arrayKeys', [$this, 'arrayKeys']));
        $this->twigService->addFunction(new \Twig_SimpleFunction('arrayValues', [$this, 'arrayValues']));

        // Widgets
        $this->twigService->addFunction(new \Twig_SimpleFunction('parceJsonData', [$this, 'parceJsonData']));
        $this->twigService->addFunction(new \Twig_SimpleFunction('widgetInput', [$this, 'widgetInput']));
        $this->twigService->addFunction(new \Twig_SimpleFunction('boxMessages', [$this, 'boxMessages']));

        return $this->twigService;
    }

    /**
     * @return mixed
     */
    public function boxMessages()
    {
        return $this->twigService->render(self::WIDGET_DIR.'/boxMessages.html.twig', ['message' => $this->messages->__toArray()]);
    }

    /**
     * @param array $data
     * @param $fieldName
     * @return mixed
     */
    public function parceJsonData(array $data, $formName, $fieldName)
    {
        return $this->twigService->render(self::WIDGET_DIR.'/parceJsonData.html.twig', ['data' => $data, 'formName' => $formName, 'fieldName' => $fieldName ]);
    }

    /**
     * @param array $array
     * @return bool
     */
    public function isArrayMulti(array $array)
    {
        return (count($array, COUNT_RECURSIVE) != count($array));
    }

    /**
     * @param array $data
     * @return array
     */
    public function arrayValues(array $data)
    {
        return array_values($data);
    }

    /**
     * @param array $data
     * @return array
     */
    public function arrayKeys(array $data)
    {
        return array_keys($data);
    }

    /**
     * @param string $value
     * @return bool
     */
    public function isString($value)
    {
        return is_string($value);
    }

    /**
     * @param array|string $value
     * @return bool
     */
    public function isArray($value)
    {
        return (is_array($value) && count($value));
    }

    /**
     * @param array $value
     * @return bool
     */
    public function isAssoc(array $value)
    {
        return ValidateTrait::isAssoc($value);
    }

    /**
     * @param string $name
     * @param string $value
     * @param bool $label
     * @return string
     */
    public function widgetInput($name, $value, $label = false)
    {
        return $this->twigService->render(self::WIDGET_DIR.'/input.html.twig', ['label' => $label, 'name' => $name, 'value' => $value ]);
    }

    /**
     * @param integer $value
     * @return string
     */
    public function queueStatus($value)
    {
        return (isset(Manifest::$statuses[$value]))?Manifest::$statuses[$value]:'None';
    }

    /**
     * @param integer $value
     * @return string
     */
    public function varDump($value)
    {
        return var_dump($value);
    }

    /**
     * @param integer $value
     * @return string
     */
    public function varPrint($value)
    {
        echo "<pre>";
        print_r($value);
        echo "</pre>";

        return true;
    }
}