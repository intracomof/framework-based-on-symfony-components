<?php

namespace LogosV8\Library\DateTime;

/**
 * Class DateTime
 *
 * @package LogosV8\Library\DateTime
 */
class DateTimeLogos
{
    /**
     * @var string
     */
    const FORMAT_SHORT = 'Y.m.d';

    /**
     * @var string
     */
    const FORMAT_FULL = 'Y.m.d H:i:s';

    /**
     * @var string
     */
    const FORMAT_CURRENT_DATE_SNAKE = 'd_m_Y';

    /**
     * @var string
     */
    const FORMAT_MYSQL = 'Y-m-d H:i:s';

    /**
     * @var string
     */
    const FORMAT_JS_MYSQL = 'YYYY-MM-dd H:m:s';

    /**
     * Get now in format
     *
     * @param string $format
     *
     * @return string
     */
    public static function now($format = self::FORMAT_MYSQL)
    {
        return (new \DateTime())->format($format);
    }

    /**
     * @param $date
     * @return int
     */
    public static function dateToTimestamp($date)
    {
        $date = \DateTime::createFromFormat("d-m-Y", $date);
        return $date->getTimestamp();
    }

    /**
     * @param $timestamp
     * @return false|string
     */
    public static function getDateTimeFromTimestamp($timestamp)
    {
        return date("Y-m-d H:i:s", $timestamp);
    }
}