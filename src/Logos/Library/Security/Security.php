<?php

namespace LogosV8\Library\Security;

use LogosV8\Library\LogosException;

class Security
{
    /**
     * @param $password
     * @param $hash
     * @return bool
     */
    public static function validatePassword($password, $hash)
    {
        if (!is_string($password) || $password === '') {
            new LogosException(LogosException::THROW_BAD_PASSWORD, LogosException::TYPE_EXCEPTION);
        }

        if (!preg_match('/^\$2[axy]\$(\d\d)\$[\.\/0-9A-Za-z]{22}/', $hash, $matches)
            || $matches[1] < 4
            || $matches[1] > 30
        ) {
            new LogosException(LogosException::THROW_BAD_HASH, LogosException::TYPE_EXCEPTION);
        }

        if (function_exists('password_verify')) {
            return password_verify($password, $hash);
        }

        $test = crypt($password, $hash);
        $n = strlen($test);

        if ($n !== 60) {
            return false;
        }

        $result = self::compareString($test, $hash);
        return $result;
    }

    /**
     * @param $password
     * @param int $cost
     * @return bool|string
     * @throws \Exception
     */
    public function generatePasswordHash($password, $cost = 13)
    {
        if (function_exists('password_hash')) {
            /** @noinspection PhpUndefinedConstantInspection */
            return password_hash($password, PASSWORD_DEFAULT, ['cost' => $cost]);
        }

        $salt = $this->generateSalt($cost);
        $hash = crypt($password, $salt);
        // strlen() is safe since crypt() returns only ascii
        if (!is_string($hash) || strlen($hash) !== 60) {
            throw new \Exception('Unknown error occurred while generating hash.');
        }

        return $hash;
    }

    /**
     * @param int $cost
     * @return string
     */
    protected function generateSalt($cost = 13)
    {
        $cost = (int) $cost;

        // Get a 20-byte random string
        $rand = $this->generateRandomKey(20);
        // Form the prefix that specifies Blowfish (bcrypt) algorithm and cost parameter.
        $salt = sprintf("$2y$%02d$", $cost);
        // Append the random salt data in the required base64 format.
        $salt .= str_replace('+', '.', substr(base64_encode($rand), 0, 22));

        return $salt;
    }

    /**
     * @param int $length
     * @return string
     */
    public function generateRandomKey($length = 32)
    {
        // always use random_bytes() if it is available
        if (function_exists('random_bytes')) {
            return random_bytes($length);
        }
    }

    /**
     * @param $expected
     * @param $actual
     * @return bool
     */
    public static function compareString($expected, $actual)
    {
        $expected .= "\0";
        $actual .= "\0";
        $expectedLength = mb_strlen($expected, '8bit');
        $actualLength = mb_strlen($actual, '8bit');
        $diff = $expectedLength - $actualLength;

        for ($i = 0; $i < $actualLength; $i++) {
            $diff |= (ord($actual[$i]) ^ ord($expected[$i % $expectedLength]));
        }

        return $diff === 0;
    }
}