<?php

namespace LogosV8\Services\Getter\ExpressWaybill;

use LogosV8\Services\ExpressWaybill\ExpressWaybillService;

/**
 * Class ExpressWaybillServiceGetter
 * @package Logos\Service\Getter
 */
trait ExpressWaybillServiceGetter
{

    protected $_expressWaybillService = null;

    /**
     * @return ExpressWaybillService
     */
    public function getExpressWaybillService()
    {
        if($this->_expressWaybillService == null) {
            $this->_expressWaybillService = new ExpressWaybillService();
        }

        return $this->_expressWaybillService;
    }
}