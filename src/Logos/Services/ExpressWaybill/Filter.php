<?php

namespace LogosV8\Services\ExpressWaybill;

/**
 * Class Filter
 * @package LogosV8\Service\ExpressWaybill
 */
class Filter
{
    const DATE_FROM_TO = 'date_from_to';

    const DATE_FROM = 'date_from';

    const DATE_TO = 'date_to';

    const EW_NUMS = 'ew_nums';

    const CARRIER_ID = 'carrier_id';

    /**
     * Фильтры для выборки накладных
     *
     * @var array
     */
    public static $filter = [
        self::DATE_FROM_TO  => [
            self::DATE_FROM,
            self::DATE_TO
        ],
        self::EW_NUMS,
        self::CARRIER_ID
    ];
}