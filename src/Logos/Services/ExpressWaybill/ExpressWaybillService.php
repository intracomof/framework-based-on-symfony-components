<?php

namespace LogosV8\Services\ExpressWaybill;

use LogosV8\Models\ExpressWaybill\ExpressWaybillModel as ExpressWaybill;
use LogosV8\Container;

/**
 * Class ExpressWaybillService
 * @package Logos\Service
 */
class ExpressWaybillService
{
    /**
     * @param string $storage
     * @param bool $caching
     * @return object
     */
    public function getExpressWaybillMapper($storage = 'mongo', $caching = false)
    {
        $component = 'ew_' . $storage;

        $component = $caching ? $component . '_caching' : $component . '_mapper';

        return Container::getContainer()->get($component);
    }
    /**
     * fetch cached EW array and fill model
     *
     * @param $ewNum
     * @return bool|ExpressWaybill
     */
    public function getExpressWaybillModelByEwNum($ewNum)
    {
        $expressWaybillModel = $this->getExpressWaybillMapper('mysql', 1)->setTags(['ew_num' => $ewNum])->fetchModelByEwNum($ewNum);

        return $expressWaybillModel;
    }

    /**
     * fetch cached EW array
     *
     * @param $ewId
     * @return mixed
     */
    public function getExpressWaybillArrayByEwId($ewId)
    {
        $expressWaybillArray = Container::get('ew_mongo_mapper')->setTags(['ew_id' => $ewId])->findById($ewId);
        return $expressWaybillArray;
    }

    /**
     * Update EW and clear cache
     *
     * @param ExpressWaybill $model
     */
    public function update(ExpressWaybill $model)
    {
        $this->getExpressWaybillMapper('mysql')->update($model, ['id' => $model->getId()]);
        $this->getExpressWaybillMapper('mysql', 1)->clearTags(['ew_num' => $model->getEwNum()]);
        $this->getExpressWaybillMapper('mysql', 1)->fetchModelByEwNum($model->getEwNum());
    }

    /**
     * @param ExpressWaybill $model
     */
    public function insert(ExpressWaybill $model)
    {
        $this->getExpressWaybillMapper('mongo')->insert($model);
    }

    /**
     * @param array $filter
     * @return bool|\MongoCursor
     */
    public static function getMongoCursorByFilter(array $filter)
    {
        $filterForMongo = [];

        foreach($filter as $key => $val) {
            switch($key) {
                case Filter::DATE_FROM:
                    $filterForMongo['date_sec']['$gte'] = $val;
                    break;
                case Filter::DATE_TO:
                    $filterForMongo['date_sec']['$lte'] = $val;
                    break;
                case Filter::CARRIER_ID:
                    $filterForMongo['carrier_ids'] = $val;
                    break;
                case Filter::EW_NUMS:
                    $filterForMongo['ew_num']['$in'] = array_map(
                        function($ewNum) {
                            return $ewNum;
                        },
                        $val
                    );
                    break;
            }
        }

        $result = Container::get('ew_mongo_mapper')->fetchExpressWaybillByFilter($filterForMongo);

        return $result;
    }
}