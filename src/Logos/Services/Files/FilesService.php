<?php

namespace LogosV8\Services\Files;

use LogosV8\Container;

class FilesService
{
    /**
     * @param $filename
     * @param $text
     */
    public static function writeToFile($filename, $text)
    {
        $text = mb_convert_encoding($text, "windows-1251", "utf-8");
        $text = html_entity_decode($text, ENT_QUOTES, 'utf-8');
        $text = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $text);

        $filename = Container::getConfig('common')['files_path']['xml']['tmp'] . $filename;

        $fp = fopen($filename, "a");
        fwrite($fp, $text);
        fclose($fp);
    }
}