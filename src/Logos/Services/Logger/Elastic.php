<?php

namespace LogosV8\Services\Logger;

use LogosV8\Container;
use LogosV8\Library\DateTime\DateTimeLogos;
use LogosV8\Storage\Elastic\ElasticStorage as Storage;


/**
 * Class Elastic
 * @package LogosV8\Service\Logger
 */
class Elastic implements ILogger
{
    /** Prefix for elastic search index */
    const SEARCH_INDEX_PREFIX = 'logos-';

    /** Default type name for elastic search */
    const TYPE_NAME = 'logger';

    /**
     * Insert to log
     * @param string $label
     * @param mixed $value
     */
    public static function log($label, $value)
    {
        Container::get('elastic_storage')->upsert($label, $value, self::getIndexName(), self::getTypeName());
    }

    /**
     * Get index name (index name for elastic search)
     *
     * @return string
     */
    static function getIndexName()
    {
        return self::SEARCH_INDEX_PREFIX . (DateTimeLogos::now(DateTimeLogos::FORMAT_SHORT));
    }

    /**
     * Get type name (index type name for elastic search)
     *
     * @return string
     */
    static function getTypeName()
    {
        return self::TYPE_NAME;
    }
}