<?php
/**
 * Created by PhpStorm.
 * User: yshkurnykov
 * Date: 06.10.2016
 * Time: 10:35
 */

namespace LogosV8\Services\Logger;

/**
 * Interface ILogger
 * @package LogosV8\Service\Logger
 */
interface ILogger
{
    /**
     * Insert to log
     * @param string $label
     * @param mixed $value
     */
    public static function log($label, $value);
}