<?php

namespace LogosV8\Mapper\ExpressWaybill;

use LogosV8\Models\ExpressWaybill\ExpressWaybillModel as ExpressWaybill;
use LogosV8\Library\LogosException;
use LogosV8\Mapper\Mapper;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class ExpressWaybillMapper
 * @package Logos\Model\Mapper
 */
class ExpressWaybillMapper extends Mapper
{
    private $data;

    public function find(array $params)
    {
        return $this->getStorage()->find($params);
    }

    public function findAll()
    {
        $this->data = $this->getStorage()->findAll();
        return $this->data;
    }

    public function findByEtdNum($num)
    {
        $result = $this->getStorage()->find(['manifest.utd_num' =>$num]);

        return $result;
    }

    public function issetEwByUtdNum($num)
    {
        return $this->getStorage()->find(['manifest.utd_num' =>$num])->count();

    }

    public function findOneBy($params)
    {
        return new ArrayCollection($this->getStorage()->findOneBy($params));
    }

    /**
     * @param $ewNum
     * @return array
     */
    public function fetchArrayByEwNum($ewNum)
    {
        $result = $this->getStorage()->fetchExpressWayBillByEwNum($ewNum);

        return $result;
    }

    /**
     * fetch model from storage without cache
     *
     * @param $ewNum
     * @return bool|ExpressWaybill
     */
    public function fetchModelByEwNum($ewNum)
    {
        $result = $this->getStorage()->fetchExpressWayBillByEwNum($ewNum);
        return $result;
    }

    /**
     * @param array $ewNums
     * @return array
     */
    public function fetchArrayByEwNums(array $ewNums)
    {
        $result = [];
        foreach ($ewNums as $ewNum)
        {
            $data = $this->fetchArrayByEwNum($ewNum);
            $result[] = $data;
        }

        return $result;
    }


    /**
     * @param array $ewIds
     * @return \MongoCursor
     */
    public function fetchExpressWaybillByIds(array $ewIds)
    {
        $result = $this->getStorage()->find([
            'id'    => [
                '$in'   => array_map(
                    function($n) {
                        return (int)$n;
                    },
                    $ewIds
                )
            ]
        ]);

        return $result;
    }

    /**
     * @param $filter
     * @return mixed
     */
    public function fetchExpressWaybillByFilter($filter)
    {
        $result = $this->getStorage()->find($filter);

        return $result;
    }

    /**
     * @param ExpressWaybill $model
     * @param $query
     * @param array $options
     * @return mixed
     */
    public function update($model, $query, array $options = [])
    {
        $ewData = $model->toArray();

        self::checkRequirementFields($ewData, $model->getRequirementFields());

        return $this->getStorage()->update($ewData, $query, $options);
    }

    /**
     * @param ExpressWaybill $model
     * @return mixed
     */
    public function insert($model)
    {
        $ewData = $model->toArray();

        // Проверка на существование обязательных полей
        self::checkRequirementFields($ewData, $model->getRequirementFields());

        return $this->getStorage()->insert($ewData);
    }

    /**
     * @param array $ewData
     * @param array $requirementFields
     */
    public static function checkRequirementFields(array $ewData, array $requirementFields)
    {
        $emptyFields = [];
        foreach($requirementFields as $reqField) {
            if(empty($ewData[$reqField]) || !array_key_exists($reqField, $ewData)) {
                $emptyFields[] = $reqField;
            }
        }

        if(!empty($emptyFields)) {
            new LogosException(LogosException::THROW_REQUIREMENT_FIELDS_ERROR, LogosException::TYPE_EXCEPTION,
                implode(", ", $emptyFields)
            );
        }

    }

    /**
     * @param array $data
     * @param array $query
     * @param array $options
     * @return mixed
     */
    public function removeTo($data, $query, array $options = [])
    {
        return $this->getStorage()->update(['$pull' => $data], $query, $options);
    }

    /**
     * @param array $data
     * @param array $query
     * @param array $options
     * @return mixed
     */
    public function appendTo($data, $query, array $options = [])
    {
        return $this->getStorage()->update(['$push' => $data], $query, $options);
    }

    /**
     * @param int $start
     * @param int $length
     * @param array $query
     * @param array $orders
     * @return mixed
     */
    public function findOffset($start = 0, $length = 25, array $query = [], array $orders = [])
    {
        $data = $this->getStorage()->findOffset($start, $length, $query, $orders);

        return new ArrayCollection(iterator_to_array($data));
    }

    /**
     * @param array $query
     * @param array $options
     * @return mixed
     */
    public function recordsTotal(array $query = [], array $options = [])
    {
        return $this->getStorage()->recordsTotal($query, $options);
    }

    /**
     * @param $params
     * @return ExpressWaybill
     */
    public function fillModel($params)
    {
        $expressWayBillModel = new ExpressWaybill();

        if(is_array($params) && !empty($params)) {
            $expressWayBillModel->setId($params['id']);
            $expressWayBillModel->setEwNum($params['ew_num']);
            $expressWayBillModel->setState($params['state']);
            $expressWayBillModel->setDate($params['date']);
            $expressWayBillModel->setEstDeliveryDate($params['est_delivery_date']);
            $expressWayBillModel->setServiceType($params['service_type']);
            $expressWayBillModel->setPrimaryNum($params['primary_num']);
            $expressWayBillModel->setPrimaryDate($params['primary_date']);
            $expressWayBillModel->setOrderNum($params['order_num']);
            $expressWayBillModel->setOrderDate($params['order_date']);
            $expressWayBillModel->setShipmentType($params['shipment_type']);
            $expressWayBillModel->setTotalDimensionalWeightKg($params['total_dimensional_weight_kg']);
            $expressWayBillModel->setTotalActualWeightKg($params['total_actual_weight_kg']);
            $expressWayBillModel->setGeneralDesc($params['general_desc']);
            $expressWayBillModel->setDeclaredCost($params['declared_cost']);
            $expressWayBillModel->setDeclaredCurrency($params['declared_currency']);
            $expressWayBillModel->setCustomsDeclarationCost($params['customs_declaration_cost']);
            $expressWayBillModel->setCustomsDeclarationCurrency($params['customs_declaration_currency']);
            $expressWayBillModel->setPayerType($params['payer_type']);
            $expressWayBillModel->setPayerThirdPartyId($params['payer_third_party_id']);
            $expressWayBillModel->setPayerPaymentType($params['payer_payment_type']);
            $expressWayBillModel->setClosingDate($params['closing_date']);
            $expressWayBillModel->setClosingSendingReceiver($params['closing_sending_receiver']);
            $expressWayBillModel->setClosingSendingReceiverId($params['closing_sending_receiver_id']);
            $expressWayBillModel->setClosingIssuedShipment($params['closing_issued_shipment']);
            $expressWayBillModel->setClosingAddShipmentInfo($params['closing_add_shipment_info']);
            $expressWayBillModel->setClosingReceiverPostId($params['closing_receiver_post_id']);
            $expressWayBillModel->setClosingReceiverDocType($params['closing_receiver_doc_type']);
            $expressWayBillModel->setClosingReceiverDocSerialNum($params['closing_receiver_doc_serial_num']);
            $expressWayBillModel->setClosingDocNum($params['closing_doc_num']);
            $expressWayBillModel->setClosingDocIssueDate($params['closing_doc_issue_date']);
            $expressWayBillModel->setClosingShipmentNotes($params['closing_shipment_notes']);
            $expressWayBillModel->setStorageExpirationDate($params['storage_expiration_date']);
            $expressWayBillModel->setIssueCssResponsiblePers($params['issue_css_responsible_pers']);
            $expressWayBillModel->setDocInfoForPackageIssue($params['doc_info_for_package_issue']);
            $expressWayBillModel->setDocInfoForCustoms($params['doc_info_for_customs']);
            $expressWayBillModel->setRecNumAllowedIssue($params['rec_num_allowed_issue']);
            $expressWayBillModel->setCreatorUserId($params['creator_user_id']);
            $expressWayBillModel->setCargoEstWeightKg($params['cargo_est_weight_kg']);
            $expressWayBillModel->setEwType($params['ew_type']);
            $expressWayBillModel->setCustomsBrokerage($params['customs_brokerage']);
            $expressWayBillModel->setPickedShipment($params['picked_shipment']);
            $expressWayBillModel->setDimenCntrlWeightKg($params['dimen_cntrl_weight_kg']);
            $expressWayBillModel->setDeliveryType($params['delivery_type']);
            $expressWayBillModel->setActualCntrlWeightKg($params['actual_cntrl_weight_kg']);
            $expressWayBillModel->setShipmentFormat($params['shipment_format']);
            $expressWayBillModel->setSenderCounterpartyId($params['sender_counterparty_id']);
            $expressWayBillModel->setSenderCpAddressId($params['sender_cp_address_id']);
            $expressWayBillModel->setSenderCpContactpersId($params['sender_cp_contactpers_id']);
            $expressWayBillModel->setSenderCpPhonenumId($params['sender_cp_phonenum_id']);
            $expressWayBillModel->setSenderCpEmailId($params['sender_cp_email_id']);
            $expressWayBillModel->setReceiverCounterpartyId($params['receiver_counterparty_id']);
            $expressWayBillModel->setReceiverCpAddressId($params['receiver_cp_address_id']);
            $expressWayBillModel->setReceiverCpContactpersId($params['receiver_cp_contactpers_id']);
            $expressWayBillModel->setReceiverCpPhonenumId($params['receiver_cp_phonenum_id']);
            $expressWayBillModel->setReceiverCpEmailId($params['receiver_cp_email_id']);
            $expressWayBillModel->setTariffZoneId($params['tariff_zone_id']);
            $expressWayBillModel->setMvsId($params['mvs_id']);
        }

        return $expressWayBillModel;
    }
}