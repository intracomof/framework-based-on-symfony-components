<?php

namespace LogosV8\Mapper\Caching\ExpressWaybill;

use LogosV8\Mapper\Caching\AbstractCaching;

/**
 * Class ExpressWaybillCaching
 * @package Logos\Model\Mapper\Caching
 */
class ExpressWaybillCaching extends AbstractCaching
{

    /**
     * @var int $_lifetime 2 days cache lifetime
     * @var int $_cacheVersion for renew all cache for Readers
     */
    protected $_lifetime = 172800;
    protected $_cacheVersion = 7;

    /**
     * @var array of methods to cache
     */
    protected $_autoCacheMethods =
        [
            'fetchArrayByEwNum'    =>
                [
                    'lifetime'  => 300,
                    'tags'      => ['ew_num']
                ],
            'fetchModelByEwNum'    =>
                [
                    'lifetime' => 300,
                    'callback' => 'fillModel',
                    'tags'      => ['ew_num']
                ],
            'fetchArrayByEwNums'   =>
                [
                    'multi' => 'fetchArrayByEwNum',
                    'lifetime' => 300,
                ],
            'findById'             =>
                [
                    'lifetime'  => 300,
                    'tags'      => ['ew_id']
                ]
        ];

    /**
     * @param \LogosV8\Mapper\ExpressWaybill\ExpressWaybillMapper $target
     */
    public function __construct($target)
    {
        parent::__construct($target);
    }

}