<?php

namespace LogosV8\Mapper\Caching;

use LogosV8\Container;
use LogosV8\Library\LogosException;
use LogosV8\Storage\Memcache\MemcacheStorage as MemcacheLogos;

/**
 * Abstract class for caching
 *
 * Class AbstractCaching
 * @package Logos\Model\Mapper\Caching
 */
abstract class AbstractCaching
{
    /**
     * Called method of mapper
     *
     * @var string
     */
    protected $_method;

    /**
     * Arguments for called method
     *
     * @var array
     */
    protected $_arguments;

    /**
     * Memcache Server Number
     *
     * @var int
     */
    protected $_serverKey = 0;

    /**
     * Is disabled caching?
     *
     * @var bool
     */
    protected $_disabledCaching = false;

    /**
     * Default cache instance
     *
     * @var MemcacheLogos
     */
    protected static $_defaultCache = null;

    /**
     * Methods allowed for auto cache
     *
     * @var array
     */
    protected $_autoCacheMethods = [];

    /**
     * Cache keys for methods
     *
     * @var array
     */
    protected $_methodKeys = [];

    /**
     * Cache version
     *
     * @var integer
     */
    protected $_cacheVersion = 0;

    /**
     * Cache version
     *
     * @var integer
     */
    protected $_lifetime = null;

    /**
     * Cache lifetime for tag
     *
     * @var integer
     */
    protected $_lifetimeTag = 604800; // 7 days

    /**
     * Key prefix for target cache
     *
     * @var string
     */
    protected $_targetCacheKeyPrefix = 'Logos_Caching_';

    /**
     * Cache instance
     *
     * @var MemcacheLogos
     */
    protected $_cache = null;

    /**
     * Instance which is cached
     *
     * @var \stdClass
     */
    protected $_target = null;

    /**
     * Max size property key
     *
     * @var integer
     */
    protected $_targetCacheKeyArgsMaxSize = 50000;

    /**
     * If mode on cache will be not load, but save to cache
     *
     * @var boolean
     */
    protected $_resetMode = false;

    /** @var array $_tags */
    protected $_tags = [];

    /** @var bool $_clearTagResult */
    protected $_clearTagResult = false;

    /** @var  bool $_clearOnceMode */
    protected $_clearOnceMode;

    /**
     * @param $target
     */
    public function __construct($target)
    {
        $this->_target = $target;
    }

    /**
     * @return int
     */
    public function getServerKey()
    {
        return $this->_serverKey;
    }

    /**
     * @param int $serverKey
     */
    public function setServerKey($serverKey)
    {
        if(is_string($serverKey) && strlen($serverKey) > 27) {
            $this->_serverKey = $serverKey[27];
        }
    }


    /**
     * @return boolean
     */
    public function getClearTagResult()
    {
        return $this->_clearTagResult;
    }

    /**
     * Disable caching
     *
     * @param bool
     * @return void
     */
    public function disableCaching($flag = true)
    {
        $this->_disabledCaching = (bool) $flag;
    }

    /**
     * Set default cache
     *
     * @param MemcacheLogos
     * @return void
     */
    public static function setDefaultCache($cache)
    {
        self::$_defaultCache = $cache;
    }

    /**
     * Setting reset mode with flag
     *
     * @param boolean $mode
     * @return \LogosV8\Mapper\Caching\AbstractCaching
     */
    public function setResetMode($mode)
    {
        $this->_resetMode = $mode;
        return $this;
    }

    /**
     * @param $mode
     * @return $this
     */
    public function setClearOnceMode($mode)
    {
        $this->_clearOnceMode = $mode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClearOnceMode()
    {
        return $this->_clearOnceMode;
    }

    /**
     * Getter: reset mode
     * @return boolean;
     */
    public function getResetMode()
    {
        return $this->_resetMode;
    }


    /**
     * Get default cache
     *
     * @return MemcacheLogos | null
     */
    public static function getDefaultCache()
    {
        return self::$_defaultCache;
    }

    /**
     * Set cache
     *
     * @param MemcacheLogos $cache
     * @return \LogosV8\Mapper\Caching\AbstractCaching
     */
    public function setCache($cache)
    {
        $this->_cache = $cache;
        return $this;
    }

    /**
     * Get cache
     *
     * @return MemcacheLogos;
     */
    public function getCache()
    {

        if ($this->_cache == null) {

            $connectionParams = Container::getConfig('memcache');
            $server = $connectionParams[$this->getServerKey()]['server'];
            $port = $connectionParams[$this->getServerKey()]['port'];

            $memcache = new \Memcached();
            $memcache->addServer($server, $port);

            $memcacheLogos = new MemcacheLogos($memcache);
            $memcacheLogos->setMemcache($memcache);

            $this->_cache = $memcacheLogos;
        }

        return $this->_cache;
    }

    /**
     * Set custom key for method
     *
     * @param string $targetMethod
     * @param string $key
     * @return \LogosV8\Mapper\Caching\AbstractCaching
     */
    public function setCustomKey($targetMethod, $key)
    {
        if (!method_exists($this->_target, $targetMethod)) {
            new LogosException(LogosException::THROW_MAPPER_ERROR, LogosException::TYPE_EXCEPTION,
                'Can not set cache key. Method ' . get_class($this->_target) . '::' . $targetMethod . '() is not defined'
            );
        }

        if (!is_string($key)) {
            new LogosException(LogosException::THROW_MAPPER_ERROR, LogosException::TYPE_EXCEPTION,
                'Can not set cache key. Required "string" type for key parameter. Now it is "' . gettype($key) . '"'
            );
        }

        $this->_methodKeys[$targetMethod] = $key;

        return $this;
    }

    /**
     * Get custom key for method
     *
     * @param $targetMethod
     * @return mixed|null
     */
    public function getCustomKey($targetMethod)
    {
        if (isset($this->_methodKeys[$targetMethod])) {
            return $this->_methodKeys[$targetMethod];
        } else {
            return null;
        }
    }

    /**
     * @param $targetMethod
     * @return $this
     */
    public function removeCustomKey($targetMethod)
    {
        if(!array_key_exists($targetMethod, $this->_methodKeys)) {
            return $this;
        }

        unset($this->_methodKeys[$targetMethod]);

        return $this;
    }

    /**
     * Get key for method
     *
     * @param $targetMethod
     * @param array $targetMethodArgs
     * @return string
     */
    public function getKey($targetMethod, array $targetMethodArgs = [])
    {
        if (empty($targetMethod)) {
            new LogosException(LogosException::THROW_MAPPER_ERROR, LogosException::TYPE_EXCEPTION,
                'Target method name can not be empty'
            );
        }

        $wrappedKey = $this->_wrapMethodKey($targetMethod, $this->_generateMethodKey($targetMethod, $targetMethodArgs));

        return $wrappedKey;
    }

    /**
     * Wrap key for method
     *
     * @param string $targetMethod
     * @param string $key
     * @return string
     */
    protected function _wrapMethodKey($targetMethod, $key)
    {
        return $this->_targetCacheKeyPrefix . get_class($this) . '_' . $targetMethod . '_' . $key . '_' . $this->_cacheVersion;
    }

    /**
     * Generate method key
     *
     * @param string $targetMethod
     * @param array $targetMethodArgs
     * @return string
     */
    protected function _generateMethodKey($targetMethod, array $targetMethodArgs = [])
    {
        $targetMethodArgsSerialized = serialize($this->_typeCast($targetMethodArgs));
        $targetKeyPropetriesSerialized = serialize($this->_typeCast($this->_getTargetKeyProperties($targetMethod)));

        if (strlen($targetKeyPropetriesSerialized) > $this->_targetCacheKeyArgsMaxSize) {
            trigger_error(
                'Properties part of cache key is too large - ' . strlen($targetKeyPropetriesSerialized),
                E_USER_WARNING
            );
        }

        if (strlen($targetMethodArgsSerialized) > $this->_targetCacheKeyArgsMaxSize) {
            trigger_error(
                'Args part of cache key is too large - ' . $targetMethod .' '. strlen($targetMethodArgsSerialized),
                E_USER_WARNING
            );
        }

        $key = md5($targetMethodArgsSerialized . $targetKeyPropetriesSerialized);

        $this->setServerKey($key);

        return $key;
    }

    /**
     * @param array $tags
     *
     * @return $this
     */
    public function clearTags(array $tags)
    {
        foreach($tags as $key => $tag) {

            $tagsId = md5($key.'_'.$tag);
            $cache = $this->getCache();

            if($cache){
                $cache->setLifetime($this->_lifetimeTag);
                $info = $cache->loadKey($tagsId);

                if (is_array($info) && isset($info[MemcacheLogos::SAVING_FLAG_NAME]) && $info[MemcacheLogos::SAVING_FLAG_NAME]) {
                    $tagsVersion = $info[MemcacheLogos::SAVING_DATA_NAME];
                    $tagsVersion++;
                    $cache->saveKey($tagsId, $tagsVersion);
                    $this->_clearTagResult = true;
                }
            }
        }

        return $this;
    }

    /**
     * @param $lifetime
     * @return $this
     */
    public function setLifetime($lifetime)
    {
        $this->_lifetime = $lifetime;
        return $this;
    }

    /**
     * @param $tags
     *
     * @return $this
     */
    public function setTags($tags)
    {
        $this->_tags = $tags;
        return $this;
    }


    /**
     * Get key properties for method
     *
     * @param $targetMethod
     * @return array
     */
    protected function _getTargetKeyProperties($targetMethod)
    {

        if (!array_key_exists($targetMethod, $this->_autoCacheMethods) || !isset($this->_autoCacheMethods[$targetMethod]['properties'])) {
            return [];
        }

        $autoProperties = $this->_autoCacheMethods[$targetMethod]['properties'];
        if (!is_array($autoProperties)) {
            new LogosException(LogosException::THROW_MAPPER_ERROR, LogosException::TYPE_EXCEPTION,
                'Auto cache target method ' . get_class($this) . '::' . $targetMethod . '() value required array type'
            );
        }

        $properties = [];
        foreach ($autoProperties as $property) {
            $preparedProperty = strpos($property, '_') === 0? substr($property, 1): $property;
            $preparedProperty = ucfirst($preparedProperty);
            $propertyGetter = 'get' . $preparedProperty;
            if (!method_exists($this->_target, $propertyGetter)) {
                new LogosException(LogosException::THROW_MAPPER_ERROR, LogosException::TYPE_EXCEPTION,
                    'Getter for property "' . $property . '" not defined. Getter - ' . $propertyGetter
                );
            }

            $properties[$property] = $this->_target->$propertyGetter();
        }

        return $properties;
    }

    /**
     * Get key properties for tag
     *
     * @param $targetMethod
     * @return array
     */
    protected function _getTargetTagsVersion($targetMethod){
        if (!array_key_exists($targetMethod, $this->_autoCacheMethods) || !isset($this->_autoCacheMethods[$targetMethod]['tags'])) {
            return [];
        }
        $autoTags = $this->_autoCacheMethods[$targetMethod]['tags'];
        if (!is_array($autoTags)) {
            new LogosException(LogosException::THROW_MAPPER_ERROR, LogosException::TYPE_EXCEPTION,
                'Auto cache target method ' . get_class($this) . '::' . $targetMethod . '() value required array type'
            );
        }

        $resultTagsVersion = [];
        $cache = $this->getCache();
        $cache->setLifetime($this->_lifetimeTag);

        foreach ($autoTags as $tag) {
            if (isset($this->_tags[$tag]) && $this->_tags[$tag])
            {
                $tagsId = md5($tag.'_'.$this->_tags[$tag]);
                $info = $cache->loadKey($tagsId);

                if (is_array($info) && isset($info[MemcacheLogos::SAVING_FLAG_NAME]) && $info[MemcacheLogos::SAVING_FLAG_NAME]) {
                    $tagsVersion = $info[MemcacheLogos::SAVING_DATA_NAME];
                }
                else{
                    $tagsVersion = time().mt_rand(0, 1000);
                    $cache->saveKey($tagsId, $tagsVersion);
                }
                $resultTagsVersion[] = $tagsVersion;
            }
        }
        return $resultTagsVersion;
    }

    /**
     * Type cast for cache key
     *
     * @param array $data
     * @return array
     */
    public function _typeCast(array $data)
    {
        $result = [];
        foreach ($data as $name => $value) {
            switch (gettype($value)) {
                case 'object':
                    if (method_exists($value, '__toString')) {
                        $result[$name] = (string) $value;
                    } else {
                        new LogosException(LogosException::THROW_MAPPER_ERROR, LogosException::TYPE_EXCEPTION,
                            'Can`t generate cache key by object value. Method ' . get_class($value) . '::__toString() is not defined'
                        );
                    }
                    break;
                case 'array':
                    $result[$name] = $value;
                    break;
                default:
                    $result[$name] = (string) $value;
                    break;
            }
        }


        return $result;
    }

    /**
     * @param $method
     * @param $arguments
     * @return array
     */
    protected function _getMultiCacheResult($method, $arguments)
    {
        /** @var MemcacheLogos $cache */
        $cache = $this->getCache();
        $oneMethod = $this->_autoCacheMethods[$method]['multi'];
        $keys = [];

        $params = array_pop($arguments);
        $keysData = [];

        foreach ($params as $param) {
            $key = $this->getKey($oneMethod, [$param]);
            $keysData[$key] = $param;
            $keys[] = $key;
        }
        $info = $cache->loadMulti($keys);
        $res = [];

        foreach($keysData as $key=>$val){
            if (is_array($info[$key]) && isset($info[$key][MemcacheLogos::SAVING_FLAG_NAME]) && $info[$key][MemcacheLogos::SAVING_FLAG_NAME]) {
                $res[] = $info[$key][MemcacheLogos::SAVING_DATA_NAME];
            }
            else {
                $res[] = $this->$oneMethod($val);
            }
        }

        return $res;
    }

    /**
     * @param $method
     * @param $arguments
     * @return mixed
     */
    protected function _getCacheResult($method, $arguments)
    {
        $cache = $this->getCache();
        $key = $this->getKey($method, $arguments);
        $info = $cache->loadKey($key);

        if (!(is_array($info) && isset($info[MemcacheLogos::SAVING_FLAG_NAME]) && $info[MemcacheLogos::SAVING_FLAG_NAME]) || $this->getResetMode()) {

            $result = call_user_func_array([$this->_target, $method], $arguments);
            $leftTime = $this->_lifetime;

            if(isset($this->_autoCacheMethods[$method]['lifetime'])){
                $leftTime=$this->_autoCacheMethods[$method]['lifetime'];
            }

            $cache->saveKey($key, $result, $leftTime);

        } else {
            $result = $info[MemcacheLogos::SAVING_DATA_NAME];
        }

        return $result;
    }

    /**
     * Default cache wrapper for target methods
     *
     * @param string $method
     * @param array $arguments
     * @param integer|false $lifetime
     * @return mixed
     */
    protected function _fetchCached($method, array $arguments = [], $lifetime = null)
    {
        $result = null;
        $key = $this->getKey($method, $arguments);

        if(!$cache = $this->getCache()) {
            return $this->_callMethod($this->_method, $this->_arguments);
        }

        if(isset($this->_autoCacheMethods[$method]) && isset($this->_autoCacheMethods[$method]['multi'])) {
            $result = $this->_getMultiCacheResult($method, $arguments);
        }

        if($this->getClearOnceMode()) {
            $cache->removeKey($key);
            $result = true;
        }

        if(!$result) {
            $result = $this->_getCacheResult($method, $arguments);
        }

        $result = $this->callbackFunction($method, $result);

        return $result;
    }

    /**
     * @param $method
     * @param $arguments
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        if (!method_exists($this->_target, $method)) {
            new LogosException(LogosException::THROW_MAPPER_ERROR, LogosException::TYPE_EXCEPTION,
                'Method "' . $method . '" is not defined in "' . get_class($this->_target) . '" target'
            );
        }

        $this->_method = $method;
        $this->_arguments = $arguments;

        if(!isset($this->_autoCacheMethods[$method]) || $this->getResetMode() || $this->getClearOnceMode()){
            return $this->_callMethod($method, $arguments);
        }

        return $this->_fetchCached($method, $arguments);
    }

    /**
     * @param $method
     * @param $result
     * @return mixed
     */
    public function callbackFunction($method, $result)
    {
        if($result !== true && isset($this->_autoCacheMethods[$method]['callback'])){
            $result = call_user_func_array([$this->_target, $this->_autoCacheMethods[$method]['callback']], [$result]);
        }

        return $result;
    }

    /**
     * @param $method
     * @param $arguments
     * @return mixed
     */
    protected function _callMethod($method, $arguments)
    {
        $result = call_user_func_array([$this->_target, $method], $arguments);
        $result = $this->callbackFunction($method, $result);

        return $result;
    }
}