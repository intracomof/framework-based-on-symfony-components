<?php

namespace LogosV8\Mapper;

use LogosV8\Storage\AbstractStorage;

/**
 * Class AbstractMapper
 * @package Logos\Model\Mapper
 */
abstract class Mapper
{
    /**
     * @var $_storage AbstractStorage
     */
    protected $storage;

    public $model_class;

    /**
     * Mapper constructor.
     * @param AbstractStorage $storage
     */
    public function __construct(AbstractStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return object $storage
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * AbstractMapper constructor.
     * @param $storage
     */
    protected function setStorage(AbstractStorage $storage)
    {
        $this->storage = $storage;

    }

    /**
     * @see AbstractCaching::setTags()
     *
     * @param array $array
     *
     * @return $this
     */
    public function setTags(array $array)
    {
        return $this;
    }

    /**
     * @see Model_Mapper_Caching_Abstract::clearTags()
     *
     * @param array $array
     *
     * @return $this
     */
    public function clearTags(array $array)
    {
        return $this;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return $this->getStorage()->findById($id);

    }

    /**
     * @return array|bool
     */
    public function findAll()
    {
        return $this->getStorage()->findAll();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function insert($data)
    {
        return $this->getStorage()->insert($data);
    }

    /**
     * @param array $data
     * @param $query
     * @return array|bool
     */
    public function update($data, $query)
    {
        return $this->getStorage()->update($data, $query);
    }

    /**
     * @param array $query
     * @param array $options
     * @return mixed
     */
    public function delete(array $query, array $options = [])
    {
        return $this->getStorage()->delete($query, $options);
    }

    /**
     * @param array $data
     * @param $query
     * @param array $options
     * @return array|bool
     */
    public function updateMulti($query, array $data, array $options = [])
    {
        return $this->getStorage()->updateMulti($query, $data, $options);
    }
}