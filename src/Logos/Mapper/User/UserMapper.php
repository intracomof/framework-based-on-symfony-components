<?php

namespace LogosV8\Mapper\User;

use LogosV8\Mapper\Mapper;
use LogosV8\Models\User\UserModel;

/**
 * Class UserMapper
 * @package Logos\Model\Mapper
 */
class UserMapper extends Mapper
{

    /**
     * @param $login
     * @return bool|UserModel
     */
    public function findByLoginName($login)
    {
        $userData = $this->getStorage()->fetchUserByLoginName($login);
        if(!empty($userData)) {
            $user = new UserModel();
            $this->loadUserModel($user, $userData);

            return $user;
        }

        return false;
    }

    /**
     * @param $id
     * @return bool|UserModel
     */
    public function findById($id)
    {
        $userData = $this->getStorage()->findById($id);
        if(!empty($userData)) {
            $user = new UserModel();
            $this->loadUserModel($user, $userData);

            return $user;
        }
        return false;
    }

    /**
     * @param UserModel $user
     * @param array $userData
     */
    public function loadUserModel(UserModel $user, array $userData)
    {
        foreach($userData as $key => $value) {
            $user->{$key} = $value;
        }
    }
}