<?php

namespace LogosV8\Mapper\Getter\ExpressWaybill;

use LogosV8\Mapper\ExpressWaybill\ExpressWaybillMapper as ExpressWaybillMapper;
use LogosV8\Mapper\Caching\ExpressWaybill\ExpressWaybillCaching as ExpressWaybillCaching;

/**
 * Class ExpressWaybillMapperGetter
 * @package Logos\Model\Mapper\Getter
 */
trait ExpressWaybillMapperGetter
{
    /** @var ExpressWaybillMapper $_expressWaybillMapper */
    protected $_expressWaybillMapper;

    /** @var ExpressWaybillMapper $_expressWaybillMapper  */
    protected $_expressWaybillCaching;

    /**
     * @param bool $storage
     * @return ExpressWaybillMapper
     */
    public function getExpressWaybillMapper($storage = false)
    {
        $expressWaybillMapper = ExpressWaybillMapper::getInstance($storage);

        return $expressWaybillMapper;
    }

    /**
     * @param bool $storage
     * @param bool $new
     * @return ExpressWaybillMapper
     */
    public function getExpressWaybillCaching($storage = false, $new = false)
    {
        if($new !== false || !($this->_expressWaybillCaching instanceof ExpressWaybillCaching)) {
            $this->_expressWaybillCaching = new ExpressWaybillCaching($this->getExpressWaybillMapper($storage));
        }

        return $this->_expressWaybillCaching;
    }
}