<?php

namespace LogosV8\Models;

/**
 * Class Model_Abstract
 * @package Logos\Model
 */
abstract class AbstractModel
{

    protected $_requirementFields = [];

    public $isNewRecord = true;

    /** @var array Fields that we don't need to insert */
    protected $_excludedFieldsByDefault = [
        "requirementFields",
        "excludedFieldsByModel",
        "excludedFieldsByDefault"
    ];

    /** @var array $_excludedFieldsByModel Fields of model that we don't need to insert */
    protected $_excludedFieldsByModel = [];


    /**
     * @return array
     */
    public function getRequirementFields()
    {
        return $this->_requirementFields;
    }

    /**
     * @return array
     */
    public function getExcludedFields()
    {
        return array_merge($this->_excludedFieldsByDefault, $this->_excludedFieldsByModel);
    }

    /**
     * Converting Model to Array
     *
     * @return array
     */
    public function toArray()
    {
        $dataArr = [];
        
        foreach($this as $key => $value) {
            $dataArr[ltrim($key, '_')] = self::castToType($value);
        }

        foreach($this->getExcludedFields() as $value) {
            unset($dataArr[$value]);
        }

        return $dataArr;
    }

    /**
     *
     * Приведение переменных к нужным типам (для того, чтоб в монге не было все строками)
     *
     * @param $var
     * @return bool|float|int
     */
    public static function castToType($var)
    {
        if(is_numeric($var)) {
            if(stristr($var, ".")) {
                return (float)$var;
            }
            return (int)$var;
        }

        if(is_bool($var))
            return (bool) $var;

        return $var;
    }
}