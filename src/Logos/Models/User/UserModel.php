<?php

namespace LogosV8\Models\User;

use LogosV8\Models\AbstractModel;

use LogosV8\Plugins\ACL\Roles;

class UserModel extends AbstractModel
{
    protected $_role = Roles::GUEST;

    /**
     * @return int
     */
    public function getRole()
    {
        return $this->_role;
    }

    /**
     * @param int $role
     */
    public function setRole($role)
    {
        $this->_role = $role;
    }

    public  $user_id,
            $login_name,
            $pass,
            $status,
            $employee_id,
            $state,
            $auth_token,
            $workstation_id;


}