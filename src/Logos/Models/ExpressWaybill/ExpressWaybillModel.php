<?php

namespace LogosV8\Models\ExpressWaybill;

use \LogosV8\Models\AbstractModel;
use LogosV8\Mapper\Getter\ExpressWaybill\ExpressWaybillMapperGetter;
use LogosV8\Storage\Mongo\ExpressWaybill\ExpressWaybillMongoStorage;

/**
 * Class ExpressWaybill
 * @package Logos\Model
 */
class ExpressWaybillModel extends AbstractModel
{

    use ExpressWaybillMapperGetter;

    /**
     * Состояние создана
     */
    const STATE_CREATED = 1;

    /*
     * Состояние закрыта
     */
    const STATE_CLOSED = 2;

    /**
     * Состояние удалена
     */
    const STATE_DELETED = 100;

    protected $_requirementFields = [
        'ew_num',
        'state',
        'customs_declaration_currency',
        'shipment_type',
        'payer_payment_type'
    ];

    protected $_excludedFieldsByModel = [];

    protected
        $_id,
        $_ew_num,
        $_state,
        $_date,
        $_est_delivery_date,
        $_service_type,
        $_primary_num,
        $_primary_date,
        $_order_num,
        $_order_date,
        $_shipment_type,
        $_total_dimensional_weight_kg,
        $_total_actual_weight_kg,
        $_general_desc,
        $_declared_cost,
        $_declared_currency,
        $_customs_declaration_cost,
        $_customs_declaration_currency,
        $_payer_type,
        $_payer_third_party_id,
        $_payer_payment_type,
        $_closing_date,
        $_closing_sending_receiver,
        $_closing_sending_receiver_id,
        $_closing_issued_shipment,
        $_closing_add_shipment_info,
        $_closing_receiver_post_id,
        $_closing_receiver_doc_type,
        $_closing_receiver_doc_serial_num,
        $_closing_doc_num,
        $_closing_doc_issue_date,
        $_closing_shipment_notes,
        $_storage_expiration_date,
        $_issue_css_responsible_pers,
        $_doc_info_for_package_issue,
        $_doc_info_for_customs,
        $_rec_num_allowed_issue,
        $_creator_user_id,
        $_cargo_est_weight_kg,
        $_ew_type,
        $_customs_brokerage,
        $_picked_shipment,
        $_dimen_cntrl_weight_kg,
        $_delivery_type,
        $_actual_cntrl_weight_kg,
        $_shipment_format,
        $_sender_counterparty_id,
        $_sender_cp_address_id,
        $_sender_cp_contactpers_id,
        $_sender_cp_phonenum_id,
        $_sender_cp_email_id,
        $_receiver_counterparty_id,
        $_receiver_cp_address_id,
        $_receiver_cp_contactpers_id,
        $_receiver_cp_phonenum_id,
        $_receiver_cp_email_id,
        $_tariff_zone_id,
        $_mvs_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return mixed
     */
    public function getEwNum()
    {
        return $this->_ew_num;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->_state;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->_date;
    }

    /**
     * @return mixed
     */
    public function getEstDeliveryDate()
    {
        return $this->_est_delivery_date;
    }

    /**
     * @return mixed
     */
    public function getServiceType()
    {
        return $this->_service_type;
    }

    /**
     * @return mixed
     */
    public function getPrimaryNum()
    {
        return $this->_primary_num;
    }

    /**
     * @return mixed
     */
    public function getPrimaryDate()
    {
        return $this->_primary_date;
    }

    /**
     * @return mixed
     */
    public function getOrderNum()
    {
        return $this->_order_num;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->_order_date;
    }

    /**
     * @return mixed
     */
    public function getShipmentType()
    {
        return $this->_shipment_type;
    }

    /**
     * @return mixed
     */
    public function getTotalDimensionalWeightKg()
    {
        return $this->_total_dimensional_weight_kg;
    }

    /**
     * @return mixed
     */
    public function getTotalActualWeightKg()
    {
        return $this->_total_actual_weight_kg;
    }

    /**
     * @return mixed
     */
    public function getGeneralDesc()
    {
        return $this->_general_desc;
    }

    /**
     * @return mixed
     */
    public function getDeclaredCost()
    {
        return $this->_declared_cost;
    }

    /**
     * @return mixed
     */
    public function getDeclaredCurrency()
    {
        return $this->_declared_currency;
    }

    /**
     * @return mixed
     */
    public function getCustomsDeclarationCost()
    {
        return $this->_customs_declaration_cost;
    }

    /**
     * @return mixed
     */
    public function getCustomsDeclarationCurrency()
    {
        return $this->_customs_declaration_currency;
    }

    /**
     * @return mixed
     */
    public function getPayerType()
    {
        return $this->_payer_type;
    }

    /**
     * @return mixed
     */
    public function getPayerThirdPartyId()
    {
        return $this->_payer_third_party_id;
    }

    /**
     * @return mixed
     */
    public function getPayerPaymentType()
    {
        return $this->_payer_payment_type;
    }

    /**
     * @return mixed
     */
    public function getClosingDate()
    {
        return $this->_closing_date;
    }

    /**
     * @return mixed
     */
    public function getClosingSendingReceiver()
    {
        return $this->_closing_sending_receiver;
    }

    /**
     * @return mixed
     */
    public function getClosingSendingReceiverId()
    {
        return $this->_closing_sending_receiver_id;
    }

    /**
     * @return mixed
     */
    public function getClosingIssuedShipment()
    {
        return $this->_closing_issued_shipment;
    }

    /**
     * @return mixed
     */
    public function getClosingAddShipmentInfo()
    {
        return $this->_closing_add_shipment_info;
    }

    /**
     * @return mixed
     */
    public function getClosingReceiverPostId()
    {
        return $this->_closing_receiver_post_id;
    }

    /**
     * @return mixed
     */
    public function getClosingReceiverDocType()
    {
        return $this->_closing_receiver_doc_type;
    }

    /**
     * @return mixed
     */
    public function getClosingReceiverDocSerialNum()
    {
        return $this->_closing_receiver_doc_serial_num;
    }

    /**
     * @return mixed
     */
    public function getClosingDocNum()
    {
        return $this->_closing_doc_num;
    }

    /**
     * @return mixed
     */
    public function getClosingDocIssueDate()
    {
        return $this->_closing_doc_issue_date;
    }

    /**
     * @return mixed
     */
    public function getClosingShipmentNotes()
    {
        return $this->_closing_shipment_notes;
    }

    /**
     * @return mixed
     */
    public function getStorageExpirationDate()
    {
        return $this->_storage_expiration_date;
    }

    /**
     * @return mixed
     */
    public function getIssueCssResponsiblePers()
    {
        return $this->_issue_css_responsible_pers;
    }

    /**
     * @return mixed
     */
    public function getDocInfoForPackageIssue()
    {
        return $this->_doc_info_for_package_issue;
    }

    /**
     * @return mixed
     */
    public function getDocInfoForCustoms()
    {
        return $this->_doc_info_for_customs;
    }

    /**
     * @return mixed
     */
    public function getRecNumAllowedIssue()
    {
        return $this->_rec_num_allowed_issue;
    }

    /**
     * @return mixed
     */
    public function getCreatorUserId()
    {
        return $this->_creator_user_id;
    }

    /**
     * @return mixed
     */
    public function getCargoEstWeightKg()
    {
        return $this->_cargo_est_weight_kg;
    }

    /**
     * @return mixed
     */
    public function getEwType()
    {
        return $this->_ew_type;
    }

    /**
     * @return mixed
     */
    public function getCustomsBrokerage()
    {
        return $this->_customs_brokerage;
    }

    /**
     * @return mixed
     */
    public function getPickedShipment()
    {
        return $this->_picked_shipment;
    }

    /**
     * @return mixed
     */
    public function getDimenCntrlWeightKg()
    {
        return $this->_dimen_cntrl_weight_kg;
    }

    /**
     * @return mixed
     */
    public function getDeliveryType()
    {
        return $this->_delivery_type;
    }

    /**
     * @return mixed
     */
    public function getActualCntrlWeightKg()
    {
        return $this->_actual_cntrl_weight_kg;
    }

    /**
     * @return mixed
     */
    public function getShipmentFormat()
    {
        return $this->_shipment_format;
    }

    /**
     * @return mixed
     */
    public function getSenderCounterpartyId()
    {
        return $this->_sender_counterparty_id;
    }

    /**
     * @return mixed
     */
    public function getSenderCpAddressId()
    {
        return $this->_sender_cp_address_id;
    }

    /**
     * @return mixed
     */
    public function getSenderCpContactpersId()
    {
        return $this->_sender_cp_contactpers_id;
    }

    /**
     * @return mixed
     */
    public function getSenderCpPhonenumId()
    {
        return $this->_sender_cp_phonenum_id;
    }

    /**
     * @return mixed
     */
    public function getSenderCpEmailId()
    {
        return $this->_sender_cp_email_id;
    }

    /**
     * @return mixed
     */
    public function getReceiverCounterpartyId()
    {
        return $this->_receiver_counterparty_id;
    }

    /**
     * @return mixed
     */
    public function getReceiverCpAddressId()
    {
        return $this->_receiver_cp_address_id;
    }

    /**
     * @return mixed
     */
    public function getReceiverCpContactpersId()
    {
        return $this->_receiver_cp_contactpers_id;
    }

    /**
     * @return mixed
     */
    public function getReceiverCpPhonenumId()
    {
        return $this->_receiver_cp_phonenum_id;
    }

    /**
     * @return mixed
     */
    public function getReceiverCpEmailId()
    {
        return $this->_receiver_cp_email_id;
    }

    /**
     * @return mixed
     */
    public function getTariffZoneId()
    {
        return $this->_tariff_zone_id;
    }

    /**
     * @return mixed
     */
    public function getMvsId()
    {
        return $this->_mvs_id;
    }

    /**
     * @param array $requirementFields
     */
    public function setRequirementFields($requirementFields)
    {
        $this->_requirementFields = $requirementFields;
    }

    /**
     * @param array $excludedFieldsByModel
     */
    public function setExcludedFieldsByModel($excludedFieldsByModel)
    {
        $this->_excludedFieldsByModel = $excludedFieldsByModel;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @param mixed $ew_num
     */
    public function setEwNum($ew_num)
    {
        $this->_ew_num = $ew_num;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->_state = $state;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->_date = $date;
    }

    /**
     * @param mixed $est_delivery_date
     */
    public function setEstDeliveryDate($est_delivery_date)
    {
        $this->_est_delivery_date = $est_delivery_date;
    }

    /**
     * @param mixed $service_type
     */
    public function setServiceType($service_type)
    {
        $this->_service_type = $service_type;
    }

    /**
     * @param mixed $primary_num
     */
    public function setPrimaryNum($primary_num)
    {
        $this->_primary_num = $primary_num;
    }

    /**
     * @param mixed $primary_date
     */
    public function setPrimaryDate($primary_date)
    {
        $this->_primary_date = $primary_date;
    }

    /**
     * @param mixed $order_num
     */
    public function setOrderNum($order_num)
    {
        $this->_order_num = $order_num;
    }

    /**
     * @param mixed $order_date
     */
    public function setOrderDate($order_date)
    {
        $this->_order_date = $order_date;
    }

    /**
     * @param mixed $shipment_type
     */
    public function setShipmentType($shipment_type)
    {
        $this->_shipment_type = $shipment_type;
    }

    /**
     * @param mixed $total_dimensional_weight_kg
     */
    public function setTotalDimensionalWeightKg($total_dimensional_weight_kg)
    {
        $this->_total_dimensional_weight_kg = $total_dimensional_weight_kg;
    }

    /**
     * @param mixed $total_actual_weight_kg
     */
    public function setTotalActualWeightKg($total_actual_weight_kg)
    {
        $this->_total_actual_weight_kg = $total_actual_weight_kg;
    }

    /**
     * @param mixed $general_desc
     */
    public function setGeneralDesc($general_desc)
    {
        $this->_general_desc = $general_desc;
    }

    /**
     * @param mixed $declared_cost
     */
    public function setDeclaredCost($declared_cost)
    {
        $this->_declared_cost = $declared_cost;
    }

    /**
     * @param mixed $declared_currency
     */
    public function setDeclaredCurrency($declared_currency)
    {
        $this->_declared_currency = $declared_currency;
    }

    /**
     * @param mixed $customs_declaration_cost
     */
    public function setCustomsDeclarationCost($customs_declaration_cost)
    {
        $this->_customs_declaration_cost = $customs_declaration_cost;
    }

    /**
     * @param mixed $customs_declaration_currency
     */
    public function setCustomsDeclarationCurrency($customs_declaration_currency)
    {
        $this->_customs_declaration_currency = $customs_declaration_currency;
    }

    /**
     * @param mixed $payer_type
     */
    public function setPayerType($payer_type)
    {
        $this->_payer_type = $payer_type;
    }

    /**
     * @param mixed $payer_third_party_id
     */
    public function setPayerThirdPartyId($payer_third_party_id)
    {
        $this->_payer_third_party_id = $payer_third_party_id;
    }

    /**
     * @param mixed $payer_payment_type
     */
    public function setPayerPaymentType($payer_payment_type)
    {
        $this->_payer_payment_type = $payer_payment_type;
    }

    /**
     * @param mixed $closing_date
     */
    public function setClosingDate($closing_date)
    {
        $this->_closing_date = $closing_date;
    }

    /**
     * @param mixed $closing_sending_receiver
     */
    public function setClosingSendingReceiver($closing_sending_receiver)
    {
        $this->_closing_sending_receiver = $closing_sending_receiver;
    }

    /**
     * @param mixed $closing_sending_receiver_id
     */
    public function setClosingSendingReceiverId($closing_sending_receiver_id)
    {
        $this->_closing_sending_receiver_id = $closing_sending_receiver_id;
    }

    /**
     * @param mixed $closing_issued_shipment
     */
    public function setClosingIssuedShipment($closing_issued_shipment)
    {
        $this->_closing_issued_shipment = $closing_issued_shipment;
    }

    /**
     * @param mixed $closing_add_shipment_info
     */
    public function setClosingAddShipmentInfo($closing_add_shipment_info)
    {
        $this->_closing_add_shipment_info = $closing_add_shipment_info;
    }

    /**
     * @param mixed $closing_receiver_post_id
     */
    public function setClosingReceiverPostId($closing_receiver_post_id)
    {
        $this->_closing_receiver_post_id = $closing_receiver_post_id;
    }

    /**
     * @param mixed $closing_receiver_doc_type
     */
    public function setClosingReceiverDocType($closing_receiver_doc_type)
    {
        $this->_closing_receiver_doc_type = $closing_receiver_doc_type;
    }

    /**
     * @param mixed $closing_receiver_doc_serial_num
     */
    public function setClosingReceiverDocSerialNum($closing_receiver_doc_serial_num)
    {
        $this->_closing_receiver_doc_serial_num = $closing_receiver_doc_serial_num;
    }

    /**
     * @param mixed $closing_doc_num
     */
    public function setClosingDocNum($closing_doc_num)
    {
        $this->_closing_doc_num = $closing_doc_num;
    }

    /**
     * @param mixed $closing_doc_issue_date
     */
    public function setClosingDocIssueDate($closing_doc_issue_date)
    {
        $this->_closing_doc_issue_date = $closing_doc_issue_date;
    }

    /**
     * @param mixed $closing_shipment_notes
     */
    public function setClosingShipmentNotes($closing_shipment_notes)
    {
        $this->_closing_shipment_notes = $closing_shipment_notes;
    }

    /**
     * @param mixed $storage_expiration_date
     */
    public function setStorageExpirationDate($storage_expiration_date)
    {
        $this->_storage_expiration_date = $storage_expiration_date;
    }

    /**
     * @param mixed $issue_css_responsible_pers
     */
    public function setIssueCssResponsiblePers($issue_css_responsible_pers)
    {
        $this->_issue_css_responsible_pers = $issue_css_responsible_pers;
    }

    /**
     * @param mixed $doc_info_for_package_issue
     */
    public function setDocInfoForPackageIssue($doc_info_for_package_issue)
    {
        $this->_doc_info_for_package_issue = $doc_info_for_package_issue;
    }

    /**
     * @param mixed $doc_info_for_customs
     */
    public function setDocInfoForCustoms($doc_info_for_customs)
    {
        $this->_doc_info_for_customs = $doc_info_for_customs;
    }

    /**
     * @param mixed $rec_num_allowed_issue
     */
    public function setRecNumAllowedIssue($rec_num_allowed_issue)
    {
        $this->_rec_num_allowed_issue = $rec_num_allowed_issue;
    }

    /**
     * @param mixed $creator_user_id
     */
    public function setCreatorUserId($creator_user_id)
    {
        $this->_creator_user_id = $creator_user_id;
    }

    /**
     * @param mixed $cargo_est_weight_kg
     */
    public function setCargoEstWeightKg($cargo_est_weight_kg)
    {
        $this->_cargo_est_weight_kg = $cargo_est_weight_kg;
    }

    /**
     * @param mixed $ew_type
     */
    public function setEwType($ew_type)
    {
        $this->_ew_type = $ew_type;
    }

    /**
     * @param mixed $customs_brokerage
     */
    public function setCustomsBrokerage($customs_brokerage)
    {
        $this->_customs_brokerage = $customs_brokerage;
    }

    /**
     * @param mixed $picked_shipment
     */
    public function setPickedShipment($picked_shipment)
    {
        $this->_picked_shipment = $picked_shipment;
    }

    /**
     * @param mixed $dimen_cntrl_weight_kg
     */
    public function setDimenCntrlWeightKg($dimen_cntrl_weight_kg)
    {
        $this->_dimen_cntrl_weight_kg = $dimen_cntrl_weight_kg;
    }

    /**
     * @param mixed $delivery_type
     */
    public function setDeliveryType($delivery_type)
    {
        $this->_delivery_type = $delivery_type;
    }

    /**
     * @param mixed $actual_cntrl_weight_kg
     */
    public function setActualCntrlWeightKg($actual_cntrl_weight_kg)
    {
        $this->_actual_cntrl_weight_kg = $actual_cntrl_weight_kg;
    }

    /**
     * @param mixed $shipment_format
     */
    public function setShipmentFormat($shipment_format)
    {
        $this->_shipment_format = $shipment_format;
    }

    /**
     * @param mixed $sender_counterparty_id
     */
    public function setSenderCounterpartyId($sender_counterparty_id)
    {
        $this->_sender_counterparty_id = $sender_counterparty_id;
    }

    /**
     * @param mixed $sender_cp_address_id
     */
    public function setSenderCpAddressId($sender_cp_address_id)
    {
        $this->_sender_cp_address_id = $sender_cp_address_id;
    }

    /**
     * @param mixed $sender_cp_contactpers_id
     */
    public function setSenderCpContactpersId($sender_cp_contactpers_id)
    {
        $this->_sender_cp_contactpers_id = $sender_cp_contactpers_id;
    }

    /**
     * @param mixed $sender_cp_phonenum_id
     */
    public function setSenderCpPhonenumId($sender_cp_phonenum_id)
    {
        $this->_sender_cp_phonenum_id = $sender_cp_phonenum_id;
    }

    /**
     * @param mixed $sender_cp_email_id
     */
    public function setSenderCpEmailId($sender_cp_email_id)
    {
        $this->_sender_cp_email_id = $sender_cp_email_id;
    }

    /**
     * @param mixed $receiver_counterparty_id
     */
    public function setReceiverCounterpartyId($receiver_counterparty_id)
    {
        $this->_receiver_counterparty_id = $receiver_counterparty_id;
    }

    /**
     * @param mixed $receiver_cp_address_id
     */
    public function setReceiverCpAddressId($receiver_cp_address_id)
    {
        $this->_receiver_cp_address_id = $receiver_cp_address_id;
    }

    /**
     * @param mixed $receiver_cp_contactpers_id
     */
    public function setReceiverCpContactpersId($receiver_cp_contactpers_id)
    {
        $this->_receiver_cp_contactpers_id = $receiver_cp_contactpers_id;
    }

    /**
     * @param mixed $receiver_cp_phonenum_id
     */
    public function setReceiverCpPhonenumId($receiver_cp_phonenum_id)
    {
        $this->_receiver_cp_phonenum_id = $receiver_cp_phonenum_id;
    }

    /**
     * @param mixed $receiver_cp_email_id
     */
    public function setReceiverCpEmailId($receiver_cp_email_id)
    {
        $this->_receiver_cp_email_id = $receiver_cp_email_id;
    }

    /**
     * @param mixed $tariff_zone_id
     */
    public function setTariffZoneId($tariff_zone_id)
    {
        $this->_tariff_zone_id = $tariff_zone_id;
    }

    /**
     * @param mixed $mvs_id
     */
    public function setMvsId($mvs_id)
    {
        $this->_mvs_id = $mvs_id;
    }

    public function save()
    {
        $this->getExpressWayBillMapper(ExpressWaybillMongoStorage::getInstance())->insert($this);
    }
}