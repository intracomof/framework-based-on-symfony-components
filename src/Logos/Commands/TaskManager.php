<?php

namespace LogosV8\Commands;

use LogosV8\Services\Queue\QTask;

class TaskManager
{
    static $TIMEOUT = 1000;

    public function actionRun()
    {
        while (true)
        {
            QTask::run();

            usleep(rand(1, self::$TIMEOUT));
        }
    }
}