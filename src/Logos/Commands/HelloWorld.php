<?php

namespace LogosV8\Commands;

use LogosV8\Services\Queue\IQTask;

class HelloWorld implements IQTask
{
    public function actionExecute()
    {
        echo "Hello World";
    }

    public function process(array $params)
    {
        return "Hi World";
    }
}