<?php

namespace LogosV8\Commands\Gearman;

use LogosV8\Container;
use LogosV8\Services\ExpressWaybill\ExportToXMLService;

class WorkerExpressWaybill
{

    /**
     *  Worker
     */
    public function actionExportEwToXML()
    {
        $worker = new \GearmanWorker();

        foreach(Container::getConfig('gearman')['servers'] as $server) {
            $worker->addServer($server['host'], $server['port']);
        }

        $worker->addFunction('export_ews_to_xml', [$this, 'exportEwsToXML']);

        while (1)
        {
            $worker->work();
        }
    }

    /**
     *  Метод, который вызывается воркером
     *
     * @param $job
     */
    public function exportEwsToXML($job)
    {
        $workload = $job->workload();
        $data = unserialize($workload);

        $filter         = $data['filter'];
        $currentDate    = $data['current_date'];
        $sessionId      = $data['session_id'];

        $exportService =  new ExportToXMLService();
        $exportService->exportEwsToXmlByFilter($filter, $currentDate, $sessionId);
    }
}