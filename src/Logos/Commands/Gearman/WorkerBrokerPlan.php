<?php

namespace LogosV8\Commands\Gearman;

use LogosV8\Container;
use LogosV8\Models\BrokerPlan\BrokerPlanEw;
use LogosV8\Services\BrokerPlan\BrokerPlanEwService;
use LogosV8\Services\Gearman\Gearman;

class WorkerBrokerPlan
{

    /**
     *  Worker
     */
    public function actionRun()
    {
        $worker = new \GearmanWorker();

        foreach(Container::getConfig('gearman')['servers'] as $server) {
            $worker->addServer($server['host'], $server['port']);
        }

        $worker->addFunction('import_ew_to_broker_plan', [$this, 'importEwToBrokerPlan']);
        $worker->addFunction('recalculate-ew', [$this, 'recalculateEw']);
        $worker->addFunction('delete-ew', [$this, 'deleteEw']);

        while (1)
        {
            $worker->work();
        }
    }

    public function importEwToBrokerPlan($job)
    {
        echo "import ew";
        $workload = $job->workload();
        $params = unserialize($workload);
        $ews = Container::get('ew_mongo_mapper')->findByEtdNum($params['utd_num']);
        $exchange = Container::get('exchange_rate_mapper');
        $service = new BrokerPlanEwService($exchange);
        $service->setBpId($params['bp_id']);
        $service->setDate($params['bp_date']);
        $model = new BrokerPlanEw($service);

        $count = 0;
        Container::get('broker_plan_elastic_mapper')->writeProgressPercent(3, $params['bp_id']);
        foreach ($ews as $item) {
            $model->importToElastic($item);
            $count++;
            if($count % 10==0){
                Container::get('broker_plan_elastic_mapper')->writeProgressPercent(($count/$params['count_ew'])*100, $params['bp_id']);
            }
        }

        $this->runSetPartySign($service, $params['bp_id']);
        Container::get('broker_plan_elastic_mapper')->writeProgressPercent(100, $params['bp_id']);

        $data = ['utd_num' => (string)$params['utd_num'], 'count_mn' => '1', 'count_ew' => (string)$params['count_ew']];
        Container::get('broker_plan_elastic_mapper')->writeUtdNumber($data, $params['bp_id']);


    }

    /**
     * Пересчитуем ЕН
     * @param $job
     */
    public function recalculateEw($job)
    {
        echo "recalculate ew";
        $workload = $job->workload();
        $params = unserialize($workload);

        $exchange = Container::get('exchange_rate_mapper');
        $service = new BrokerPlanEwService($exchange);
        $service->setBpId($params['bp_id']);
        $service->setDate($params['bp_date']);

        //счетчик сколько ЕН обработано
        $count = 0;
        //записываем начало прогресс бара
        Container::get('broker_plan_elastic_mapper')->writeProgressPercent(3, $params['bp_id']);

        try{
            while(true){
                $ews = Container::get('ew_elastic_mapper')->findEwByBpId($params['bp_id'], $count, 1000);
                $total = ($ews['hits']['total']) ? $ews['hits']['total'] : 0;

                if (empty($ews['hits']['hits'])) {
                    break;
                }
                foreach ($ews['hits']['hits'] as $item) {
                    $service->recalculateEw($item['_source']);
                    $count++;
                    if($count % 100==0){
                        $percent = (($count/$total)*100)-1;
                        Container::get('broker_plan_elastic_mapper')->writeProgressPercent($percent, $params['bp_id']);
                    }
                }
            }
            $this->runSetPartySign($service, $params['bp_id']);
        } catch (\Exception $e) {
            Container::get('broker_plan_elastic_mapper')->writeProgressPercent(100, $params['bp_id']);
            echo $e->getMessage();
        }

        //var_dump($params);
        Container::get('broker_plan_elastic_mapper')->setBpDate($params['bp_id'], $params['bp_date']);
        Container::get('broker_plan_elastic_mapper')->writeProgressPercent(100, $params['bp_id']);
    }

    /**
     * Установка признака партии блокируем ЕН в которых сумма больше лиммита
     * @param $service
     * @param $bp_id
     */
    public function runSetPartySign($service, $bp_id)
    {
        echo "run set party".PHP_EOL;
        $ew_hash = Container::get('ew_elastic_mapper')->findSimilarEw($bp_id);
        //var_dump($ew_hash);
        $service->setPartySign($ew_hash);
    }

    /**
     * Удаление ЕН
     * @param $job
     */
    public function deleteEw($job)
    {
        echo "delete ew";
        $workload = $job->workload();
        $params = unserialize($workload);

        $count = 1;
        //записываем начало прогресс бара
        Container::get('broker_plan_elastic_mapper')->writeProgressPercent(3, $params['bp_id']);
        while($count){
            $data = Container::get('ew_elastic_mapper')->findByUtdNum($params['utd_num'],$params['bp_id']);
            $total = ($data['hits']['total']) ? $data['hits']['total'] : 0;
            if (!empty($data['hits']['hits'])) {
                foreach ($data['hits']['hits'] as $item){
                    unset($item['_source']);
                    try{
                        Container::get('ew_elastic_mapper')->delete($item,[]);
                    } catch (\Exception $e) {
                        echo $e->getMessage(). PHP_EOL;
                        continue;
                    }
                    $count++;
                    if($count % 10==0){
                        $percent = (($count/$total)*100)/2;
                        Container::get('broker_plan_elastic_mapper')->writeProgressPercent($percent, $params['bp_id']);
                    }
                }
            }else{
                $count = 0;
            }
        }

        Gearman::doBackground('recalculate-ew', ['utd_num' => $params['utd_num'], 'bp_id' => $params['bp_id'], 'bp_date' => $params['bp_date']]);

       // Container::get('broker_plan_elastic_mapper')->writeProgressPercent(100, $params['bp_id']);
    }


}