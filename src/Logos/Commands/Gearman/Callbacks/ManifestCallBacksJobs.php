<?php

namespace LogosV8\Commands\Gearman\Callbacks;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use LogosV8\Library\Event\Jobs\GearmanClientCallbackCompleteEvent;
use LogosV8\Library\Event\Jobs\GearmanClientCallbackDataEvent;
use LogosV8\Library\Event\GearmanEvents;

class ManifestCallBacksJobs {

    /**
     * @var EventDispatcherInterface
     *
     * Event dispatcher
     */
    protected $eventDispatcher;
    /**
     * Construct method
     *
     * @param EventDispatcherInterface $eventDispatcher Event dispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param array $data
     * @param null $contextReference
     */
    public function assignCompleteCallback($data, $contextReference = null)
    {
        $event = new GearmanClientCallbackCompleteEvent($data);
        if (!is_null($contextReference)) {
            $event->setContext($contextReference);
        }
        $this->eventDispatcher->dispatch(
            GearmanEvents::GEARMAN_CLIENT_CALLBACK_COMPLETE,
            $event
        );
    }

    /**
     * @param array $data
     */
    public function assignDataCallback($data, $handle)
    {
        $event = new GearmanClientCallbackDataEvent($data, $handle);
        $this->eventDispatcher->dispatch(
            GearmanEvents::GEARMAN_CLIENT_CALLBACK_DATA,
            $event
        );
    }
}