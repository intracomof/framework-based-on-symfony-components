<?php

namespace LogosV8\Commands\Gearman\Callbacks;

use GearmanTask;
use LogosV8\Library\Gearman\GearmanCallbacksDispatcher;

use LogosV8\Library\Event\GearmanClientCallbackCompleteEvent;
use LogosV8\Library\Event\GearmanClientCallbackCreatedEvent;
use LogosV8\Library\Event\GearmanClientCallbackDataEvent;
use LogosV8\Library\Event\GearmanClientCallbackExceptionEvent;
use LogosV8\Library\Event\GearmanClientCallbackFailEvent;
use LogosV8\Library\Event\GearmanClientCallbackStatusEvent;
use LogosV8\Library\Event\GearmanClientCallbackWarningEvent;
use LogosV8\Library\Event\GearmanClientCallbackWorkloadEvent;
use LogosV8\Library\Event\GearmanEvents;

class ManifestCallBacks extends GearmanCallbacksDispatcher {

    /**
     * @param GearmanTask $gearmanTask
     * @param null $contextReference
     */
    public function assignCompleteCallback(GearmanTask $gearmanTask, $contextReference = null)
    {
        $event = new GearmanClientCallbackCompleteEvent($gearmanTask);
        if (!is_null($contextReference)) {
            $event->setContext($contextReference);
        }
        $this->eventDispatcher->dispatch(
            GearmanEvents::GEARMAN_CLIENT_CALLBACK_COMPLETE,
            $event
        );
    }

    /**
     * @param GearmanTask $gearmanTask
     */
    public function assignFailCallback(GearmanTask $gearmanTask)
    {
        $event = new GearmanClientCallbackFailEvent($gearmanTask);
        $this->eventDispatcher->dispatch(
            GearmanEvents::GEARMAN_CLIENT_CALLBACK_FAIL,
            $event
        );
    }

    /**
     * @param GearmanTask $gearmanTask
     */
    public function assignDataCallback(GearmanTask $gearmanTask)
    {
        $event = new GearmanClientCallbackDataEvent($gearmanTask);
        $this->eventDispatcher->dispatch(
            GearmanEvents::GEARMAN_CLIENT_CALLBACK_DATA,
            $event
        );
    }

    /**
     * @param GearmanTask $gearmanTask
     */
    public function assignCreatedCallback(GearmanTask $gearmanTask)
    {
        $event = new GearmanClientCallbackCreatedEvent($gearmanTask);
        $this->eventDispatcher->dispatch(
            GearmanEvents::GEARMAN_CLIENT_CALLBACK_CREATED,
            $event
        );
    }

    /**
     * @param GearmanTask $gearmanTask
     */
    public function assignExceptionCallback(GearmanTask $gearmanTask)
    {
        $event = new GearmanClientCallbackExceptionEvent($gearmanTask);
        $this->eventDispatcher->dispatch(
            GearmanEvents::GEARMAN_CLIENT_CALLBACK_EXCEPTION,
            $event
        );
    }

    /**
     * @param GearmanTask $gearmanTask
     */
    public function assignStatusCallback(GearmanTask $gearmanTask)
    {
        $event = new GearmanClientCallbackStatusEvent($gearmanTask);
        $this->eventDispatcher->dispatch(
            GearmanEvents::GEARMAN_CLIENT_CALLBACK_STATUS,
            $event
        );
    }

    /**
     * @param GearmanTask $gearmanTask
     */
    public function assignWarningCallback(GearmanTask $gearmanTask)
    {
        $event = new GearmanClientCallbackWarningEvent($gearmanTask);
        $this->eventDispatcher->dispatch(
            GearmanEvents::GEARMAN_CLIENT_CALLBACK_WARNING,
            $event
        );
    }

    /**
     * @param GearmanTask $gearmanTask
     */
    public function assignWorkloadCallback(GearmanTask $gearmanTask)
    {
        $event = new GearmanClientCallbackWorkloadEvent($gearmanTask);
        $this->eventDispatcher->dispatch(
            GearmanEvents::GEARMAN_CLIENT_CALLBACK_WORKLOAD,
            $event
        );
    }
}