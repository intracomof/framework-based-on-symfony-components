<?php

namespace LogosV8\Commands\Gearman;

use LogosV8\Container;
use LogosV8\Services\Observer\Observer;
use LogosV8\Services\Manifest\Listeners\Jobs\ManifestCompleteListener;
use LogosV8\Services\Manifest\Listeners\Jobs\ManifestCreatedListener;
use LogosV8\Services\Manifest\Listeners\Jobs\ManifestDataListener;
use LogosV8\Library\Event\GearmanEvents;
use LogosV8\Commands\Gearman\Callbacks\ManifestCallBacksJobs;
use LogosV8\Services\Manifest\ManifestGearmanService;
use LogosV8\Models\Manifest\ManifestGearmanQueue;
use LogosV8\Services\Manifest\MnQueueService;
use LogosV8\Services\Manifest\ManifestEwsService;
use LogosV8\Services\Manifest\ManifestService;
use LogosV8\Models\Manifest\ManifestMultiEws;
use LogosV8\Models\ExpressWaybill\ExpressWaybillModel;

class WorkerManifest
{
    const DEBUG = false;
    const TIMEOUT = 0;
    const TIMEOUT_JOB = 0;
    const MINIMUM_EXECUTION_TIME = 0;

    const RETURN_CODE = 'ok';

    /**
     * @var \DateTime
     */
    protected $time;

    protected $obServer;
    protected $manifestCallBacksJobs;

    /**
     *  Worker
     */
    public function actionRun()
    {
        $worker = new \GearmanWorker();

        $this->obServer = new Observer();
        $this->doListeners();
        $this->manifestCallBacksJobs = new ManifestCallBacksJobs($this->obServer);

        foreach(Container::getConfig('gearman')['servers'] as $server) {
            $worker->addServer($server['host'], $server['port']);
        }

        $worker->addFunction('manifest_relation_ew_ids', [$this, 'manifestRelationEwIds']);
        $worker->addFunction('manifest_exception_ew_ids', [$this, 'manifestExceptionEwIds']);
        $worker->addFunction('manifest_add_to_ew', [$this, 'manifestAddToEw']);
        $worker->addFunction('manifest_remove_to_ew', [$this, 'manifestRemoveToEw']);
        $worker->addFunction('manifest_multi_upload_ews', [$this, 'manifestMultiUploadEws']);

        if (self::TIMEOUT > 0)
            $worker->setTimeout(self::TIMEOUT);

        $this->time = time();

        while (1) {
            $worker->work();
        }
    }

    /**
     * @param \GearmanJob $job
     */
    public function manifestMultiUploadEws(\GearmanJob $job)
    {
        if (self::DEBUG)
            echo "Received job: " . $job->handle() . "\n";

        $workload = $job->workload();
        $data = unserialize($workload);

        $mongoManifestMapper = Container::getContainer()->get('manifest_mongo_mapper');
        $manifestMultiEwsMapper = Container::get('manifest_multi_ews_mongo_mapper');

        if ($manifest = $mongoManifestMapper->findOneBy(['mn_id' => $data['mn_id']])->current())
        {
            $manifestEwService = new ManifestEwsService();
            $mnQueueService = new MnQueueService();

            $ewIds = [];
            $count = count($data['ews_upload']);

            $iteration = 1;
            foreach ($data['ews_upload'] as $index => $ewNum) {

                $validate = true;
                $appendData = [];
                $ew = $manifestEwService->getEwNum($ewNum);
                if ($ew->getId()) {
                    if (in_array($ew->getId(), $manifest['ews'])) {
                        $validate = false;
                        $appendData['ews_existed'] = $ewNum;
                    }

                    if ($ew->getState() == ExpressWaybillModel::STATE_CLOSED) {
                        $validate = false;
                        $appendData['ews_closed'] = $ewNum;
                    }

                    if ($ew->getState() == ExpressWaybillModel::STATE_DELETED) {
                        $validate = false;
                        $appendData['ews_deleted'] = $ewNum;
                    }

                    if ($validate) {
                        $ewIds[$index] = $ew->getId();
                        $appendData['ews_done'] = $ewNum;
                    }
                } else {
                    $appendData['ews_not_found'] = $ewNum;
                }

                $manifestMultiEwsMapper->removeTo(['ews_upload' => ['$in' => [$ewNum]]], ['id' => $data['id']]);
                $manifestMultiEwsMapper->appendTo($appendData, ['id' => $data['id']]);
                $manifestMultiEwsMapper->update(['$set' => ['processing' => round(($iteration/$count)*100)]], ['id' => $data['id']]);
                sleep(1);

                $iteration++;
            }

            $mongoManifestMapper->update(['$set' => ['ews' => array_merge($manifest['ews'], $ewIds)]], ['mn_id' => $manifest['mn_id']]);
            (new ManifestService())->recalculateWeight($manifest['mn_id']);
            $mnQueueService->QtaskMassLogEvent(MnQueueService::EW_LINK_MN, $ewIds);
            $mnQueueService->QtaskMassSendDimForNewEws($ewIds);
        }
    }

    /**
     * Manifest RelationEwIds
     * @param \GearmanJob $job
     */
    public function manifestRelationEwIds(\GearmanJob $job)
    {
        if (self::DEBUG)
            echo "Received job: " . $job->handle() . "\n";

        $workload = $job->workload();
        $workload_size = $job->workloadSize();
        $data = unserialize($workload);

        if (self::DEBUG)
            echo "Workload: $workload ($workload_size)\n";

        if(is_array($data['ews'])) {
            foreach ($data['ews'] as $iteration => $ewId) {
                // sendData
                $this->manifestCallBacksJobs->assignDataCallback(
                    [
                        'ewId' => $ewId,
                        'mnId' => $data['mnId'],
                        'count' => count($data['ews']),
                        'iteration' => ++$iteration
                    ], $job->handle());
            }
        }

        // sendComplete
        $this->manifestCallBacksJobs->assignCompleteCallback($data);

        if (self::TIMEOUT_JOB > 0)
            sleep(self::TIMEOUT_JOB);

        /**
         * If there is a minimum expected duration, wait out the remaining period if there is any.
         */
        if (self::MINIMUM_EXECUTION_TIME > 0) {
            $now = time();
            $remaining = self::MINIMUM_EXECUTION_TIME - ($now - $this->time);
            if ($remaining > 0) {
                sleep($remaining);
            }
        }
    }

    /**
     * @param \GearmanJob $job
     * @throws \Exception
     */
    public function manifestRemoveToEw(\GearmanJob $job)
    {
        if (self::DEBUG)
            echo "Received job: " . $job->handle() . "\n";

        $workload = $job->workload();
        $data = unserialize($workload);

        $mongoManifestMapper = Container::getContainer()->get('manifest_mongo_mapper');
        $mongoEwMapper = Container::getContainer()->get('ew_mongo_mapper');

        if ($ew = $mongoEwMapper->findOneBy(['id' => $data['ewId']])->current()
            && $manifest = $mongoManifestMapper->findOneBy(['mn_id' => $data['mnId']])->current()
        ) {
            $mongoEwMapper->removeTo(['manifest' => ['mn_num' => $manifest['mn_num']]], ['id' => $data['ewId']]);
        }
    }

    /**
     * @param \GearmanJob $job
     */
    public function manifestAddToEw(\GearmanJob $job)
    {
        if (self::DEBUG)
            echo "Received job: " . $job->handle() . "\n";

        $workload = $job->workload();
        $data = unserialize($workload);

        $mongoManifestMapper = Container::getContainer()->get('manifest_mongo_mapper');
        $mongoEwMapper = Container::getContainer()->get('ew_mongo_mapper');

        if ($ew = $mongoEwMapper->findOneBy(['id' => $data['ewId']])->current()
            && $mongoManifestMapper->findOneBy(['mn_id' => $data['mnId']])->count()
        ) {
            $insert = true;
            foreach ($ew['manifest'] as $key => $manifestInEw) {
                if ( $manifestInEw['mn_num'] == $data['data']['mn_num'] ) {
                    $mongoEwMapper->updateMulti(['$set' => ['manifest.'.$key => $data['data']], ['id' => $data['ewId']]]);
                    $insert = false;
                }
            }

            if ($insert)
                $mongoEwMapper->appendTo(['manifest' => $data['data']], ['id' => $data['ewId']]);

        }
    }

    /**
     * @param \GearmanJob $job
     */
    public function manifestExceptionEwIds(\GearmanJob $job)
    {
        if (self::DEBUG)
            echo "Received job: " . $job->handle() . "\n";

        $workload = $job->workload();
        $workload_size = $job->workloadSize();
        $data = unserialize($workload);

        $statusQueue = ManifestGearmanQueue::STATUS_ERROR;
        $updateFields = [];

        if (self::DEBUG)
            echo "Workload: $workload ($workload_size)\n";

        $mongoManifestMapper = Container::getContainer()->get('manifest_mongo_mapper');
        $mongoEwMapper = Container::getContainer()->get('ew_mongo_mapper');
        $mongoManifestQueueMapper = Container::getContainer()->get('manifest_queue_mongo_mapper');

        while (true) {
            if ($mongoEwMapper->findOneBy(['ew_num' => $data['ewId']])->count()
                && $manifest = $mongoManifestMapper->findOneBy(['mn_id' => $data['mnId']])->current()) {

                $statusQueue = ManifestGearmanQueue::STATUS_DONE;
                $ew = $mongoEwMapper->findOneBy(['ew_num' => $data['ewId']])->current();

                $updateFields = [
                    'ews' => array_merge($manifest['ews'], [$ew['id']]),
                    'queue_ews' => array_diff($manifest['queue_ews'], [$data['ewId']]),
                    'queue_parcent' => ($data['iteration'] / $data['count']) * 100,
                ];

                if ($result = (new ManifestDataListener)->validate($ew)) {
                    $statusQueue = $result;
                    unset($updateFields['ews']);
                }

                (new MnQueueService)->QtaskMassLogEvent(MnQueueService::EW_LINK_MN, [$ew['id']]);
                (new MnQueueService)->QtaskMassSendDimForNewEws([$ew['id']]);
                (new ManifestGearmanService)->addManifestToEws([$ew['id']], $manifest['mn_id'], $manifest);

                break;
            }
        }

        if (count($updateFields))
            $mongoManifestMapper->update(['$set' => $updateFields], ['mn_id' => $data['mnId']]);

        $mongoManifestQueueMapper->update(
            [
                '$set' => [
                    'status' => $statusQueue,
                    'finished_at' => date('Y-m-d H:i:s'),
                ]
            ],
            ['mn_id' => $data['mnId'], 'ew_id' => $data['ewId'], 'index' => $data['index']]
        );
    }

    /**
     * Add Gearman Listeners
     */
    private function doListeners()
    {
        $manifestDataListener = new ManifestDataListener();
        $this->obServer->addListener(GearmanEvents::GEARMAN_CLIENT_CALLBACK_DATA, [$manifestDataListener, 'run'], 1);

        $manifestCompleteListener = new ManifestCompleteListener();
        $this->obServer->addListener(GearmanEvents::GEARMAN_CLIENT_CALLBACK_COMPLETE, [$manifestCompleteListener, 'run'], 2);

        $manifestCreatedListener = new ManifestCreatedListener();
        $this->obServer->addListener(GearmanEvents::GEARMAN_CLIENT_CALLBACK_CREATED, [$manifestCreatedListener, 'run'], 3);
    }
}