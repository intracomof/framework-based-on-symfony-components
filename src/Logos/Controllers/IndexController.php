<?php

namespace LogosV8\Controllers;

/**
 * Class IndexController
 * @package LogosV8\Controllers
 */
class IndexController extends AbstractRenderingController
{

    public function indexAction()
    {
        return $this->render('index/index.html.twig', ['title' => 'Main']);
    }
}