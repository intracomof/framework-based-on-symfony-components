<?php

namespace LogosV8\Controllers;

use LogosV8\Container;
use Symfony\Component\HttpFoundation\Response;
use LogosV8\Library\Twig\TwigFunctions;

/**
 * Class AbstractController
 */
abstract class AbstractRenderingController extends AbstractController
{
    /**
     * @param string    $template
     * @param mixed     $templateVars
     * @param int       $status
     * @param array     $headers
     *
     * @return Response
     */
    public function render($template, $templateVars = [], $status = 200, $headers = [])
    {
        /** @var \Twig_Environment $twig */
        $twig = (new TwigFunctions(Container::getConfig('twig')))->run();
        $template = $twig->loadTemplate($template);

        if(Container::$user->login_name) {
            $templateVars['username'] = Container::$user->login_name;
        }

        return new Response($template->render((array)$templateVars), $status, $headers);
    }
}