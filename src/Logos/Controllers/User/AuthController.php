<?php

namespace LogosV8\Controllers\User;

use LogosV8\Container;
use LogosV8\Controllers\AbstractRenderingController;
use LogosV8\Library\Security\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class AuthController
 * @package LogosV8\Controllers\User
 */
class AuthController extends AbstractRenderingController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        if(($login = $this->getRequest()->get('login')) && ($password = $this->getRequest()->get('password'))) {
            $this->authUser($login, $password);
            return new RedirectResponse('/');
        } else {
            return $this->render('user/login.html.twig', ['title' => 'Авторизация']);
        }

    }

    /**
     * @return RedirectResponse
     */
    public function logoutAction()
    {
        Container::$session->remove('uid');
        return new RedirectResponse('/');
    }

    /**
     * @param $login
     * @param $password
     * @return bool
     */
    protected function authUser($login, $password)
    {
        $user = Container::get('user_mysql_mapper')->findByLoginName($login);
        if($user) {
            if(!Security::validatePassword($password, $user->pass)) {
                return false;
            } else {
                Container::$session->set('uid', $user->user_id);
                Container::$user = $user;
            }
        }

    }
}