<?php

namespace LogosV8\Controllers;

use LogosV8\Library\LogosException;
use LogosV8\Container;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AbstractController
 */
abstract class AbstractController
{
    protected $_permissions = [];

    protected $_request;

    /**
     * @param $action
     */
    public function checkPermission($action)
    {
        if(array_key_exists($action, $this->_permissions) &&
            Container::$user->getRole() != $this->_permissions[$action])
        {
            new LogosException(LogosException::THROW_PERMISSION_DENIED, LogosException::TYPE_EXCEPTION);
        }

    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->_request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->_request;
    }
}