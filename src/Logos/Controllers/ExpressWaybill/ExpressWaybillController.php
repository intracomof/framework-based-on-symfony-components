<?php

namespace LogosV8\Controllers\ExpressWaybill;

use LogosV8\Controllers\AbstractRenderingController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use \LogosV8\Services\Logger\Elastic as ElasticLogger;
use LogosV8\Plugins\ACL\Roles;

use LogosV8\Services\Getter\ExpressWaybill\ExpressWaybillServiceGetter;

/**
 * Class ExpressWaybillController
 * @package Logos\Controller
 */
class ExpressWaybillController extends AbstractRenderingController
{

    use ExpressWaybillServiceGetter;

    /** @var array $permissions */
    protected $_permissions = [
        'ewJsonAction' => Roles::GUEST
    ];

    /**
     * @param $ewId
     * @return JsonResponse
     */
    public function ewJsonAction($ewId)
    {
        $ewArray = $this->getExpressWaybillService()->getExpressWaybillArrayByEwId($ewId);
        return new JsonResponse($ewArray);
    }
}
