<?php

$connectionParams = [
    'wrapperClass' => 'LogosV8\Storage\MySQL\MasterSlave',
    'driverOptions' => [
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
    ],
    'driver' => 'pdo_mysql',
    'master' => [
        'dbname' => 'npi',
        'user' => 'web',
        'password' => 'npi_user',
        'host' => '172.30.23.195'
    ],
    'slaves' => [
        '172.30.23.195' => [
            'dbname' => 'npi',
            'user' => 'web',
            'password' => 'npi_user',
            'host' => '172.30.23.195',
            'driver' => 'pdo_mysql'
        ],
        '172.30.23.1' => [
            'dbname' => 'npi',
            'user' => 'web',
            'password' => 'npi_user',
            'host' => '172.30.23.195',
            'driver' => 'pdo_mysql'
        ]
    ],

    'weight_params' => [
        'master'            => [0, 100],
        '172.30.23.195'     => [101, 500],
        '172.30.23.1'       => [501, 1201]
    ],
    'min_weight' => 1,
    'max_weight' => 1200,

    'memcache_bad_servers_list' => [
        'server'    => '127.0.0.1',
        'port'      => 11211,
        'ttl'       => 3600
    ]
];

return $connectionParams;