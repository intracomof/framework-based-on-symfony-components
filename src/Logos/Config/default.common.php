<?php

$config = [
    'files_path' =>
        [
            'general'   => __DIR__ . '/../../../files/',
            'xml'       => [
                'general'   => __DIR__ . '/../../../files/xml/',
                'tmp'       => __DIR__ . '/../../../files/xml/tmp/'
            ]
        ]
];

return $config;