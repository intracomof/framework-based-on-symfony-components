<?php
require_once __DIR__ . '/../../../vendor/twig/twig/lib/Twig/Autoloader.php';

\Twig_Autoloader::register();

$loader = new \Twig_Loader_Filesystem(__DIR__ . '/../Views/');
$twig = new \Twig_Environment($loader, [
        //'cache'             =>  __DIR__ . '/../../../cache/twig',
        'auto_reload'       =>  true
    ]
);
$twig->addExtension(new Twig_Extension_StringLoader());

return $twig;