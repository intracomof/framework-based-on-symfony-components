<?php

use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();

$routes->add('base', new Routing\Route('/', [
    'ewId' => null,
    '_controller' => 'LogosV8\Controllers\IndexController::indexAction',
]));

$routes->add('ew-json', new Routing\Route('/ew-json/{ewId}', [
    'ewId' => null,
    '_controller' => 'LogosV8\Controllers\ExpressWaybill\ExpressWaybillController::ewJsonAction',
]));

//===== User Login / Logout ========
$routes->add('login', new Routing\Route('/login', [
    '_controller' => 'LogosV8\Controllers\User\AuthController::loginAction',
]));
$routes->add('logout', new Routing\Route('/logout', [
    '_controller' => 'LogosV8\Controllers\User\AuthController::logoutAction',
]));

return $routes;