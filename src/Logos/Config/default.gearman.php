<?php

$servers = [
    [
        'host'  => '172.30.23.197',
        'port'  => 4730
    ],
    [
        'host'  => '172.30.23.197',
        'port'  => 4730
    ],
];

$gearmanClient = new \GearmanClient();

foreach ($servers as $server) {
    $gearmanClient->addServer($server['host'], $server['port']);
}

$gearman = [
    'client'    => $gearmanClient,
    'servers'   => $servers
];


return $gearman;