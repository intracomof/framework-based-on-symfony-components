<?php
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Definition;

/**
 *  ExpressWaybill Mongo
 */
// EW Mongo Storage
$ewMongoStorageDefinition = new Definition('\LogosV8\Storage\Mongo\ExpressWaybill\ExpressWaybillMongoStorage');
$container->setDefinition('ew_mongo_storage', $ewMongoStorageDefinition);

// EW Mongo Mapper
$ewMapperDefinition = new Definition('\LogosV8\Mapper\ExpressWaybill\ExpressWaybillMapper', [new Reference('ew_mongo_storage')]);
$container->setDefinition('ew_mongo_mapper', $ewMapperDefinition);

// EW Mongo Mapper (Cached)
$ewCachingDefinition = new Definition('\LogosV8\Mapper\Caching\ExpressWaybill\ExpressWaybillCaching', [new Reference('ew_mongo_mapper')]);
$container->setDefinition('ew_mongo_caching', $ewCachingDefinition);

/**
 *  ExpressWaybill MySQL
 */

// EW MySQL Storage
$ewMysqlStorageDefinition = new Definition('\LogosV8\Storage\MySQL\ExpressWaybill\ExpressWaybillMySQLStorage');
$container->setDefinition('ew_mysql_storage', $ewMysqlStorageDefinition);

// EW MySQL Mapper
$ewMapperDefinition = new Definition('\LogosV8\Mapper\ExpressWaybill\ExpressWaybillMapper', [new Reference('ew_mysql_storage')]);
$container->setDefinition('ew_mysql_mapper', $ewMapperDefinition);

// EW MySQL Mapper (Cached)
$ewCachingDefinition = new Definition('\LogosV8\Mapper\Caching\ExpressWaybill\ExpressWaybillCaching', [new Reference('ew_mysql_mapper')]);
$container->setDefinition('ew_mysql_caching', $ewCachingDefinition);


/**
 * Elastic Storage
 */

$elasticStorageDefinition = new Definition('\LogosV8\Storage\Elastic\ElasticStorage');
$container->setDefinition('elastic_storage', $elasticStorageDefinition);


/**
 *  Queue Elastic Task Manager
 */

$queueMapperDefinition = new Definition('\LogosV8\Mapper\Queue\QueueElasticMapper', [new Reference('elastic_storage')]);
$container->setDefinition('queue_elastic_mapper', $queueMapperDefinition);

/**
 * Broker plan
 */
$container->setDefinition('broker_plan_elastic_storage', new Definition('\LogosV8\Storage\Elastic\BrokerPlan\BrokerPlanElasticStorage'));
$container->setDefinition('broker_plan_elastic_mapper', new Definition('\LogosV8\Mapper\BrokerPlan\BrokerPlanMapper', [new Reference('broker_plan_elastic_storage')]));

$container->setDefinition('broker_plan_ew_elastic_storage', new Definition('\LogosV8\Storage\Elastic\BrokerPlan\BrokerPlanEwElasticStorage'));

$container->setDefinition('ew_elastic_mapper', new Definition('\LogosV8\Mapper\BrokerPlan\BrokerPlanEwMapper', [new Reference('broker_plan_ew_elastic_storage')]));

/**
 * ExchangeRate
 */
$container->setDefinition('exchange_rate_storage', new Definition('\LogosV8\Storage\MySQL\ExchangeRate\ExchangeRateMySqlStorage'));
$container->setDefinition('exchange_rate_mapper', new Definition('\LogosV8\Mapper\Currency\ExchangeRateMapper', [new Reference('exchange_rate_storage')]));



// ExportEW mongo storage
$exportEwMongoStorageDefinition = new Definition('\LogosV8\Storage\Mongo\ExpressWaybill\ExportExpressWaybillMongoStorage');
$container->setDefinition('export_ew_mongo_storage', $exportEwMongoStorageDefinition);

// ExportEW Mongo Mapper
$exportEwMapperDefinition = new Definition('\LogosV8\Mapper\ExpressWaybill\ExportExpressWaybillMapper', [new Reference('export_ew_mongo_storage')]);
$container->setDefinition('export_ew_mongo_mapper', $exportEwMapperDefinition);

/**
 * Manifest
 */
$container->setDefinition('manifest_elastic_storage', new Definition('\LogosV8\Storage\Elastic\Manifest\ManifestElasticStorage'));
$container->setDefinition('manifest_elastic_mapper', new Definition('\LogosV8\Mapper\Manifest\ManifestMapper', [new Reference('manifest_elastic_storage')]));

/**
 * Manifest Mongo Storage
 */
$manifestMongoStorageDefinition = new Definition('\LogosV8\Storage\Mongo\Manifest\ManifestMongoStorage');
$container->setDefinition('manifest_mongo_storage', $manifestMongoStorageDefinition);

/**
 * Manifest Mongo Mapper
 */
$manifestMapperDefinition = new Definition('\LogosV8\Mapper\Manifest\ManifestMapper', [new Reference('manifest_mongo_storage')]);
$container->setDefinition('manifest_mongo_mapper', $manifestMapperDefinition);

/**
 * Manifest Queue Mongo Storage
 */
$manifestQueueMongoStorageDefinition = new Definition('\LogosV8\Storage\Mongo\Manifest\ManifestGearmanQueueMongoStorage');
$container->setDefinition('manifest_queue_mongo_storage', $manifestQueueMongoStorageDefinition);

/**
 * Manifest Queue Mongo Mapper
 */
$manifestQueueMapperDefinition = new Definition('\LogosV8\Mapper\Manifest\ManifestQueueMapper', [new Reference('manifest_queue_mongo_storage')]);
$container->setDefinition('manifest_queue_mongo_mapper', $manifestQueueMapperDefinition);

/**
 *  ManifestMultiEws
 */
$manifestMultiEwsMapperDefinition = new Definition('\LogosV8\Mapper\Manifest\ManifestMultiEwsMapper', [new Reference('manifest_multi_ews_mongo_storage')]);
$container->setDefinition('manifest_multi_ews_mongo_mapper', $manifestMultiEwsMapperDefinition);

$manifestMultiEwsMongoStorageDefinition = new Definition('\LogosV8\Storage\Mongo\Manifest\ManifestMultiEwsQueueMongoStorage');
$container->setDefinition('manifest_multi_ews_mongo_storage', $manifestMultiEwsMongoStorageDefinition);

/**
 *  MnQueue
 */
// MnQueue Storage
$mnQueueStorageDefinition = new Definition('\LogosV8\Storage\MySQL\MnQueue\MnQueueStorage');
$container->setDefinition('mnQueue_mysql_storage', $mnQueueStorageDefinition);

// MnQueue Mapper
$mnQueueMapperDefinition = new Definition('\LogosV8\Mapper\MnQueue\MnQueueMapper', [new Reference('mnQueue_mysql_storage')]);
$container->setDefinition('mnQueue_mysql_mapper', $mnQueueMapperDefinition);

/**
 *  ListEvents
 */
// ListEvents Storage
$listEventsStorageDefinition = new Definition('\LogosV8\Storage\MySQL\ListEvents\ListEventsStorage');
$container->setDefinition('listEvents_mysql_storage', $listEventsStorageDefinition);

// ListEvents Mapper
$listEventsMapperDefinition = new Definition('\LogosV8\Mapper\ListEvents\ListEventsMapper', [new Reference('listEvents_mysql_storage')]);
$container->setDefinition('listEvents_mysql_mapper', $listEventsMapperDefinition);

/**
 *  MnHistoryEvents
 */
// MnHistoryEvents Storage
$mnHistoryEventsStorageDefinition = new Definition('\LogosV8\Storage\MySQL\MnHistoryEvents\MnHistoryEventsStorage');
$container->setDefinition('mnHistoryEvents_mysql_storage', $mnHistoryEventsStorageDefinition);

// MnHistoryEvents Mapper
$mnHistoryEventsMapperDefinition = new Definition('\LogosV8\Mapper\MnHistoryEvents\MnHistoryEventsMapper', [new Reference('mnHistoryEvents_mysql_storage')]);
$container->setDefinition('mnHistoryEvents_mysql_mapper', $mnHistoryEventsMapperDefinition);

/**
 *  List City MySQL
 */

// List City MySQL Storage
$listCityMysqlStorageDefinition = new Definition('\LogosV8\Storage\MySQL\City\CityStorage');
$container->setDefinition('city_mysql_storage', $listCityMysqlStorageDefinition);

// List City MySQL Mapper
$listCityMapperDefinition = new Definition('\LogosV8\Mapper\City\CityMapper', [new Reference('city_mysql_storage')]);
$container->setDefinition('city_mysql_mapper', $listCityMapperDefinition);

// List City MySQL Mapper (Cached)
$cityCachingDefinition = new Definition('\LogosV8\Mapper\Caching\City\CityCaching', [new Reference('city_mysql_mapper')]);
$container->setDefinition('city_mysql_caching', $cityCachingDefinition);

/**
 *  List Counterparty MySQL
 */

// List Counterparty MySQL Storage
$listCounterpartyMysqlStorageDefinition = new Definition('\LogosV8\Storage\MySQL\Counterparty\CounterpartyStorage');
$container->setDefinition('counterparty_mysql_storage', $listCounterpartyMysqlStorageDefinition);

// List Counterparty MySQL Mapper
$listCounterpartyMapperDefinition = new Definition('\LogosV8\Mapper\Counterparty\CounterpartyMapper', [new Reference('counterparty_mysql_storage')]);
$container->setDefinition('counterparty_mysql_mapper', $listCounterpartyMapperDefinition);

// List Counterparty MySQL Mapper (Cached)
$counterpartyCachingDefinition = new Definition('\LogosV8\Mapper\Caching\Counterparty\CounterpartyCaching', [new Reference('counterparty_mysql_mapper')]);
$container->setDefinition('counterparty_mysql_caching', $counterpartyCachingDefinition);

/**
 *  List Carrier MySQL
 */

// List Carrier MySQL Storage
$listCarrierMysqlStorageDefinition = new Definition('\LogosV8\Storage\MySQL\Carrier\CarrierStorage');
$container->setDefinition('carrier_mysql_storage', $listCarrierMysqlStorageDefinition);

// List Carrier MySQL Mapper
$listCarrierMapperDefinition = new Definition('\LogosV8\Mapper\Carrier\CarrierMapper', [new Reference('carrier_mysql_storage')]);
$container->setDefinition('carrier_mysql_mapper', $listCarrierMapperDefinition);

// List Carrier MySQL Mapper (Cached)
$carrierCachingDefinition = new Definition('\LogosV8\Mapper\Caching\Carrier\CarrierCaching', [new Reference('carrier_mysql_mapper')]);
$container->setDefinition('carrier_mysql_caching', $carrierCachingDefinition);

/**
 *  List Workstation MySQL
 */

// List Workstation MySQL Storage
$listWorkstationMysqlStorageDefinition = new Definition('\LogosV8\Storage\MySQL\Workstation\WorkstationStorage');
$container->setDefinition('workstation_mysql_storage', $listWorkstationMysqlStorageDefinition);

// List Workstation MySQL Mapper
$listWorkstationMapperDefinition = new Definition('\LogosV8\Mapper\Workstation\WorkstationMapper', [new Reference('workstation_mysql_storage')]);
$container->setDefinition('workstation_mysql_mapper', $listWorkstationMapperDefinition);

/**
 *  User
 */
// User Storage
$userStorageDefinition = new Definition('\LogosV8\Storage\MySQL\User\UserMySQLStorage');
$container->setDefinition('user_mysql_storage', $userStorageDefinition);

// User Mapper
$userMapperDefinition = new Definition('\LogosV8\Mapper\User\UserMapper', [new Reference('user_mysql_storage')]);
$container->setDefinition('user_mysql_mapper', $userMapperDefinition);

/**
 * NextStatus
 */
// NextStatus Mongo Storage
$NextStatusMongoStorageDefinition = new Definition('\LogosV8\Storage\Mongo\NextStatus\NextStatusMongoStorage');
$container->setDefinition('next_status_mongo_storage', $NextStatusMongoStorageDefinition);

// NextStatus Mongo Mapper
$NextStatusMapperDefinition = new Definition('\LogosV8\Mapper\NextStatus\NextStatusMapper', [new Reference('next_status_mongo_storage')]);
$container->setDefinition('next_status_mongo_mapper', $NextStatusMapperDefinition);

// Counterparty Contact Storage
$counterpartyStorageDefinition = new Definition('\LogosV8\Storage\MySQL\Next\CounterpartyContactMySQLStorage');
$container->setDefinition('counterparty_contact_mysql_storage', $counterpartyStorageDefinition);

// Counterparty Contact Mapper
$counterpartyMapperDefinition = new Definition('\LogosV8\Mapper\NextStatus\CounterpartyContactMapper', [new Reference('counterparty_contact_mysql_storage')]);
$container->setDefinition('counterparty_contact_mysql_mapper', $counterpartyMapperDefinition);

// Counterparty Contact Storage
$counterpartyIdStorageDefinition = new Definition('\LogosV8\Storage\MySQL\Next\CounterpartyIdMySQLStorage');
$container->setDefinition('counterparty_id_mysql_storage', $counterpartyIdStorageDefinition);

// Counterparty Contact Mapper
$counterpartyIdMapperDefinition = new Definition('\LogosV8\Mapper\NextStatus\CounterpartyIdMapper', [new Reference('counterparty_id_mysql_storage')]);
$container->setDefinition('counterparty_id_mysql_mapper', $counterpartyIdMapperDefinition);

//List Statuses Ew Storage
$listStatusesEwStorageDefinition = new Definition('\LogosV8\Storage\MySQL\Next\ListStatusesEwMySQLStorage');
$container->setDefinition('list_statusesew_mysql_storage', $listStatusesEwStorageDefinition);

//List Statuses Ew Mapper
$listStatusesEwMapperDefinition = new Definition('\LogosV8\Mapper\NextStatus\ListStatusesEwMapper', [new Reference('list_statusesew_mysql_storage')]);
$container->setDefinition('list_statusesew_mysql_mapper', $listStatusesEwMapperDefinition);

/**
 *  Shipment Registry Orders MYSQL
 */
$shipmentRegistryOrdersStorageDefinition = new Definition('\LogosV8\Storage\MySQL\ShipmentRegistryOrders\ShipmentRegistryOrdersStorage');
$container->setDefinition('shipmentregistryorders_mysql_storage', $shipmentRegistryOrdersStorageDefinition);

$shipmentRegistryOrdersMapperDefinition = new Definition('\LogosV8\Mapper\ShipmentRegistryOrders\ShipmentRegistryOrdersMapper', [new Reference('shipmentregistryorders_mysql_storage')]);
$container->setDefinition('shipmentregistryorders_mysql_mapper', $shipmentRegistryOrdersMapperDefinition);