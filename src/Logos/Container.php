<?php

namespace LogosV8;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\Config\FileLocator;
use LogosV8\Library\LogosException;

/**
 * Class Config
 * @package LogosV8
 */
class Container
{
    /** Path to configs' directory */
    const CONFIG_DIR = 'Config';

    const DI_CONTAINER_FILE_CONF = 'Container/services.php';

    /** @var array $config */
    public static $config = [];

    /** @var  \LogosV8\Models\User\UserModel $user */
    public static $user;

    /** @var  \Symfony\Component\HttpFoundation\Session\Session $session */
    public static $session;

    /** @var  ContainerBuilder $container */
    public static $container;

    /**
     * Including and then returning needed config
     *
     * @param $configName
     * @return mixed
     */
    public static function getConfig($configName)
    {
        if(!array_key_exists($configName, self::$config)) {

            $fileConfig =  __DIR__ . '/' . self::CONFIG_DIR . '/' . $configName . '.php';

            if(file_exists($fileConfig)) {
                self::$config[$configName] = include $fileConfig;
            } else {
                new LogosException(LogosException::THROW_CONFIG_NOT_FOUND, LogosException::TYPE_EXCEPTION, $fileConfig);
            }
        }

        return self::$config[$configName];
    }

    /**
     *  Загружаем DI-container в Container
     */
    public static function load()
    {
        $container = new ContainerBuilder();
        $loader = new PhpFileLoader($container, new FileLocator(__DIR__ . '/' . self::CONFIG_DIR));
        $loader->load(self::DI_CONTAINER_FILE_CONF);

        self::$container = $container;
    }

    /**
     *  Get instance of Dependency Container
     *
     * @return ContainerBuilder
     */
    public static function getContainer()
    {
        if(!self::$container instanceof ContainerBuilder) {
            self::load();
        }

        return self::$container;
    }

    /**
     * Get object from Dependency Container
     *
     * @param $param
     * @return object
     */
    public static function get($param)
    {
        if(!self::$container instanceof ContainerBuilder) {
            self::load();
        }

        return self::$container->get($param);
    }
}